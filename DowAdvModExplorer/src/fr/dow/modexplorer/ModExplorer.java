package fr.dow.modexplorer;

import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.UIManager;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.modexplorer.gui.RecentModMenu;
import fr.dow.modexplorer.gui.modfoldertable.ModFolderTable;
import fr.dow.modexplorer.gui.modtree.ModTree;
import fr.dow.modexplorer.gui.parameters.ParametersDialog;
import fr.dow.modexplorer.gui.replacetext.ReplaceTextDialog;
import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel;
import fr.dow.modexplorer.modexplorerdata.ModExplorerData;
import fr.dow.modexplorer.utils.console.MessageConsoleDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.border.TitledBorder;

public class ModExplorer {

	private static final String windowTitle="Mod Explorer v0.05 - ";
	
	private static final FileNameExtensionFilter dowModFileFilter =
			new FileNameExtensionFilter("Dawn of War Mod File (module)","module");
	
	//
	// Static data
	//
	
	private static File startFile;
	private static File currentDirectory;
	
	//
	// Data
	//
	
	private DowMod dowMod;
	private final ModExplorerData modExplorerData = new ModExplorerData();;
	
	//
	// Swing components
	//
	
	private final JFrame frmModExplorer = new JFrame();
	private final ModFolderTable tableModFolder = new ModFolderTable();
	private final ModTree treeMod = new ModTree();

	private JLabel lblNewLabel;

	private JCheckBoxMenuItem chckbxmntmLowerVersions;
	private JCheckBoxMenuItem chckbxmntmLowerMod;
	private JCheckBoxMenuItem chckbxmntmArchived;
	
	private JButton btnLowVersion;
	private JButton btnLowMod;
	private JButton btnArchive;
	
	private TitledBorder dowFileVersionViewerBorder;
	private final DowFileVersionViewerPanel dowFileVersionViewerPanel = new DowFileVersionViewerPanel();

	protected MessageConsoleDialog messageConsoleDialog = new MessageConsoleDialog(); 
	
	//
	// Methods
	//
	
	static void setCurrentDirectory(File newDirectory) {
		if (newDirectory == null) return;
		if (!newDirectory.isDirectory()) return;
		currentDirectory = newDirectory;
	}

	public void openFile(File modFile) {
		
		setCurrentDirectory(modFile.getParentFile());

		frmModExplorer.setTitle(windowTitle);
		
		System.out.println();
		System.out.println("----------------------------------------------------------");
		System.out.println();
	
		dowMod=DowMod.buildFromModuleFile(modFile, true);
		
    	if (dowMod==null) return;
    	
		treeMod.setMod(modFile.getName(), dowMod);
		
		frmModExplorer.setTitle(windowTitle + " Mod = " + dowMod.getModFile().getName() + " - Directory = " + dowMod.getModFolder().getAbsolutePath());
		modExplorerData.recentModsList.addFile(dowMod.getModFile());
	}
	
	public static File createFile(DowFileVersion dowFileVersion, DowMod dowMod) {
		String fileName = dowMod.getAbsoluteFilePath(dowFileVersion.getDowFile().getDowPath());
		File fileOut = new File(fileName);
		
		if (!fileOut.getParentFile().exists()) {
			boolean ret = fileOut.getParentFile().mkdirs();
			if (!ret) {
				System.err.println("Failed to create directory : "+fileOut.getParentFile().getAbsolutePath());
				JOptionPane.showMessageDialog(null, "Could not create parent folder\n"+fileOut.getParentFile().getAbsolutePath(), "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
		}
		
		try {
			FileOutputStream fos = new FileOutputStream(fileOut);
			fos.write(dowFileVersion.getData());
			fos.close();
		} catch (IOException e) {
			System.err.println("Failed to write file : "+fileName);
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Could not save file\n"+fileName, "Warning", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
		dowFileVersion.getDowFile().addDiscFile(fileOut);
		
		System.out.println("Creation done ="+fileName);
		
		return fileOut;
	}

	
	//
	// Menu commands
	//
	
	protected void cmdMenuOpen() {
		
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(currentDirectory);
		chooser.setFileFilter(dowModFileFilter);

		int res = chooser.showOpenDialog(frmModExplorer);
		if (res != JFileChooser.APPROVE_OPTION) return;

		File sgFile = chooser.getSelectedFile();
		if (sgFile == null) {
			System.err.println("No file selected");
			return;
		}

		if (!sgFile.exists()) {
			System.err.println("File does not exist");
			return;
		}
		
		openFile(sgFile);
		
		
	}
	
	protected void cmdMenuExit() {
    	modExplorerData.save();
		System.exit(0);
	}

	protected void cmdActionCreateFiles() {

		// single file selection
		DowFileVersion selVersion = tableModFolder.getSelectedDowFileVersion();
		if (selVersion!=null) {
			if (!fileExistsInMod(selVersion, dowMod))
				if (confirmFileCreationDialog(selVersion)) {
					createFile(selVersion, dowMod);
					updateSelectedNodeDisplay();
				}
			return;
		}
		
		DowFolder selFolder = treeMod.getLastSelectedDowFolder();
		if (selFolder!=null) {
			if (confirmAllFilesInDirectoryCreationDialog(selFolder))
				System.out.println("Create all files in "+selFolder);
			return;
		}
		
	}
	
	protected void cmdActionRunFile() {
		DowFileVersion selVersion = tableModFolder.getSelectedDowFileVersion();
		if (selVersion==null) return;
		if (selVersion.getFile()==null) return;
		if(!fileExistsInMod(selVersion, dowMod)) return;
		execCommand("cmd /C \""+selVersion.getFile().getAbsolutePath()+"\"");
	}
	
	protected void cmdActionOpenExplorer() {
		
		File dirToOpen=null;
		
		DowFolder dowFolder = treeMod.getLastSelectedDowFolder();
		
		if (dowFolder==null) return;
		
		if (dowFolder!=null) {
			dirToOpen = new File(dowMod.getAbsoluteFilePath(dowFolder.getDowPath()));
		}
		
		DowFileVersion selVersion = tableModFolder.getSelectedDowFileVersion();
		if (selVersion!=null) 
		 if (selVersion.getFile()!=null)
			dirToOpen = selVersion.getFile().getParentFile();
		
		if (dirToOpen==null) return;
		
		if (!dirToOpen.exists()) {
			if (confirmDirectoryCreationDialog(dirToOpen)) {
				if (!dirToOpen.mkdir()) {;
					JOptionPane.showMessageDialog(frmModExplorer, "Creation failed \n"+dirToOpen.getAbsolutePath(), "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
			} else return;
		}
		
		if (!dirToOpen.isDirectory()) return;
		
		execCommand("explorer.exe \""+dirToOpen.getAbsolutePath()+"\"");
		
		
	}
	
	protected void cmdActionMassReplace() {
		DowFolder dowFolder = treeMod.getLastSelectedDowFolder();
		if (dowFolder==null) return;
		ReplaceTextDialog.showDialog(frmModExplorer, dowMod, dowFolder);
	}
	
	protected void cmdRefresh() {
		ModTree.ExpansionState expState = treeMod.new ExpansionState();
		Integer row = treeMod.getSelectionRow();
		if (dowMod!=null) openFile(dowMod.getModFile());
		expState.load(); 
		treeMod.setSelectionRow(row);
	}
	
	protected void cmdParameters() {
		ParametersDialog.showDialog(frmModExplorer, modExplorerData);
	}

	protected void cmdShowConsole() {
		messageConsoleDialog.setVisible(true);
	}
	
	protected void menuSwitchArchivedFiles() {
		updateSelectedNodeDisplay();
	}

	protected void menuSwitchLowerMods() {
		updateSelectedNodeDisplay();
	}

	protected void menuSwitchLowerFiles() {
		updateSelectedNodeDisplay();
	}

	protected void cmdSwitchDispArchive() {
		chckbxmntmArchived.setSelected(!chckbxmntmArchived.isSelected());
		updateSelectedNodeDisplay();
	}

	protected void cmdSwitchDispLowMod() {
		chckbxmntmLowerMod.setSelected(!chckbxmntmLowerMod.isSelected());
		updateSelectedNodeDisplay();
	}

	protected void cmdSwitchDispLowVersion() {
		chckbxmntmLowerVersions.setSelected(!chckbxmntmLowerVersions.isSelected());
		updateSelectedNodeDisplay();		
	}

	//
	// Tree selection listener
	//
		
	private final TreeSelectionListener treeListener = new TreeSelectionListener() {
		public void valueChanged(TreeSelectionEvent e) {
			updateSelectedNodeDisplay();
		}
	};
	
	private final ListSelectionListener tableSelectionListener = new ListSelectionListener() {
		@Override
	    public void valueChanged(ListSelectionEvent listSelectionEvent){
	        if (listSelectionEvent.getValueIsAdjusting()) return;
	        dowFileVersionViewerPanel.setDowFileVersion(tableModFolder.getSelectedDowFileVersion());
	        if (tableModFolder.getSelectedDowFileVersion()!=null)
	        	dowFileVersionViewerBorder.setTitle("File viewer = "+tableModFolder.getSelectedDowFileVersion().getDowFile().getDowPath());
	        else
	        	dowFileVersionViewerBorder.setTitle("File viewer");
	    }
	};
	
	private final ModFolderTable.DblClickListener dblClickListener = new ModFolderTable.DblClickListener() {
		public void dblClicked(ModFolderTable.UpdateDblClickEvent evt) {
			
			DowFileVersion dowFileVersion = evt.getDowFileVersion();
			 
			if (!fileExistsInMod(dowFileVersion, dowMod)) {
				if (!confirmFileCreationDialog(dowFileVersion)) return;
				createFile(dowFileVersion,dowMod);
				updateSelectedNodeDisplay();
				return;
			}
			
			execCommand("cmd /C \""+dowFileVersion.getFile().getAbsolutePath()+"\"");
			
		}
	};

    //
	// Constructor
	//
	
	public ModExplorer(File startFile) {
		initialize();
		if (startFile != null) openFile(startFile);
	}

	//
	// Launch application
	//
	
	public static void main(String[] args) {
		
		if (args.length > 0) {
			
			startFile = new File(args[0]);
			
			if (startFile.isFile()) {
				setCurrentDirectory(new File(startFile.getParent()));
			}
			else {
				setCurrentDirectory(startFile);
				startFile = null;
			}
			
		}

		EventQueue.invokeLater(new Runnable() {

			public void run() {

				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				}
				catch (Exception e) {
					e.printStackTrace();
				}

				try {
					ModExplorer window = new ModExplorer(startFile);
					window.frmModExplorer.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	//
	// Utilities
	//
	
	private Boolean fileExistsInMod(DowFileVersion dowFileVersion, DowMod dowMod) {
		
		if (dowFileVersion.getFile()==null) return false;
		
		return checkParent(dowFileVersion.getFile(), dowMod.getModFolder()); 	
		
	}
	
	private Boolean checkParent(File maybeChild, File possibleParent) {
		
		if (maybeChild==null) return false;
		if (possibleParent==null) return false;
		
		return maybeChild.getAbsolutePath().startsWith(possibleParent.getAbsolutePath()+File.separator);
		
	}
	
	private void execCommand(String cmd) {
		try {
			System.out.println("Running ="+cmd);
			Runtime rt = Runtime.getRuntime();
			rt.exec(cmd);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Could execute\n"+cmd, "Warning", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	//
	// GUI initialisation
	//
	
	private void updateSelectedNodeDisplay() {
		
		btnArchive.setSelected(chckbxmntmArchived.isSelected());
		btnLowVersion.setSelected(chckbxmntmLowerVersions.isSelected());
		btnLowMod.setSelected(chckbxmntmLowerMod.isSelected());

		DowFolder dowFolder = treeMod.getLastSelectedDowFolder();
		if (dowFolder != null) {
			tableModFolder.setFolder(dowFolder, dowMod,chckbxmntmLowerVersions.isSelected(), chckbxmntmLowerMod.isSelected(), chckbxmntmArchived.isSelected());
			lblNewLabel.setText(dowFolder.getDowPath());
		}
		else {
			tableModFolder.setFolder(null, null, chckbxmntmLowerVersions.isSelected(), chckbxmntmLowerMod.isSelected(), chckbxmntmArchived.isSelected());
			lblNewLabel.setText("<No File selected>");
		}
	}
	
	private boolean confirmFileCreationDialog(DowFileVersion dowFileVersion) {
		return (
			JOptionPane.showConfirmDialog(
					frmModExplorer, 
					"Create file ?\n" + dowMod.getAbsoluteFilePath(dowFileVersion.getDowFile().getDowPath()), 
					"Create file",
					JOptionPane.OK_CANCEL_OPTION
					)
			== JOptionPane.OK_OPTION);

	}
	
	private boolean confirmDirectoryCreationDialog(File directory) {
		return (
			JOptionPane.showConfirmDialog(
					frmModExplorer, 
					"Directory does not exist,\n\nCreate directory ?\n" + directory.getAbsolutePath(), 
					"Create file",
					JOptionPane.OK_CANCEL_OPTION
					)
			== JOptionPane.OK_OPTION);
	}
	
	private boolean confirmAllFilesInDirectoryCreationDialog(DowFolder dowFolder) {
		return (
			JOptionPane.showConfirmDialog(
				frmModExplorer,
				"Create all sub-files and sub-directories that DO NOT ALREADY EXIST in \n" 	+ dowMod.getAbsoluteFilePath(dowFolder.getDowPath()),
				"Create file", 
				JOptionPane.OK_CANCEL_OPTION
				)
			== JOptionPane.OK_OPTION);
	}

	private void initialize() {
		
		frmModExplorer.setTitle(windowTitle);
		frmModExplorer.setBounds(100, 100, 1049, 564);
		frmModExplorer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frmModExplorer.addWindowListener(new WindowAdapter()   {
        	public void windowClosing(WindowEvent e)
        	{
        		cmdMenuExit();
        	}
        });
		
		JMenuBar menuBar = new JMenuBar();
		frmModExplorer.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmOpenMod = new JMenuItem("Open mod");
		mntmOpenMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdMenuOpen();
			}
		});
		mnFile.add(mntmOpenMod);
		
		RecentModMenu mnNewMenu = new RecentModMenu("Recent mods",modExplorerData.recentModsList, 
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String actionCommand=e.getActionCommand();
					if (RecentModMenu.textNoMod.equals(actionCommand)) return; 
					openFile(new File(actionCommand)); 
				}
			}
		);
		mnFile.add(mnNewMenu.getJMenu());
		
		JSeparator separator = new JSeparator();
		mnFile.add(separator);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdMenuExit();
			}
		});
		mnFile.add(mntmExit);
		
		JMenu mnDisplay = new JMenu("Display");
		menuBar.add(mnDisplay);
		
		JMenuItem mntmRefresh = new JMenuItem("Refresh");
		mntmRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdRefresh();
			}
		});
		mnDisplay.add(mntmRefresh);
		
		JSeparator separator_1 = new JSeparator();
		mnDisplay.add(separator_1);
		
		chckbxmntmLowerVersions = new JCheckBoxMenuItem("Lower Files Versions");
		chckbxmntmLowerVersions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuSwitchLowerFiles();
			}
		});
		chckbxmntmLowerVersions.setSelected(true);
		mnDisplay.add(chckbxmntmLowerVersions);
		
		chckbxmntmLowerMod = new JCheckBoxMenuItem("Lower Mods");
		chckbxmntmLowerMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuSwitchLowerMods();
			}
		});
		chckbxmntmLowerMod.setSelected(true);
		mnDisplay.add(chckbxmntmLowerMod);
		
		chckbxmntmArchived = new JCheckBoxMenuItem("Archived Files");
		chckbxmntmArchived.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuSwitchArchivedFiles();
			}
		});
		chckbxmntmArchived.setSelected(true);
		mnDisplay.add(chckbxmntmArchived);
		
		JSeparator separator_2 = new JSeparator();
		mnDisplay.add(separator_2);
		
		JMenuItem mntmParameters = new JMenuItem("Parameters");
		mntmParameters.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdParameters();
			}
		});
		mnDisplay.add(mntmParameters);
		
		JMenuItem mntmShowConsole = new JMenuItem("Console");
		mntmShowConsole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdShowConsole();
			}
		});
		mnDisplay.add(mntmShowConsole);
		
		JMenu mnAction = new JMenu("Action");
		menuBar.add(mnAction);
		
		JMenuItem mntmExtractFile = new JMenuItem("Create files");
		mntmExtractFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdActionCreateFiles();
			}
		});
		mnAction.add(mntmExtractFile);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Open file (default OS)");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdActionRunFile();
			}
		});
		mnAction.add(mntmNewMenuItem);
		
		JMenuItem mntmOpenExplorerWindow = new JMenuItem("Open Explorer Window");
		mntmOpenExplorerWindow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdActionOpenExplorer();
			}
		});
		mnAction.add(mntmOpenExplorerWindow);
		
		JMenuItem mntmMassReplaceString = new JMenuItem("Mass replace string");
		mntmMassReplaceString.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdActionMassReplace();
			}
		});
		mnAction.add(mntmMassReplaceString);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.3);
		frmModExplorer.getContentPane().add(splitPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Mod folders", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		splitPane.setLeftComponent(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane);
		
		scrollPane.setViewportView(treeMod.getTree());
		treeMod.addTreeSelectionListener(treeListener);
		
		lblNewLabel = new JLabel();
		lblNewLabel.setText("<Empty>");
		panel.add(lblNewLabel, BorderLayout.SOUTH);
		
		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setResizeWeight(0.3);
		splitPane.setRightComponent(splitPane_1);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Folder content", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		splitPane_1.setLeftComponent(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		panel_2.add(tableModFolder);
		tableModFolder.addDblClickListener(dblClickListener);
		tableModFolder.addListSelectionListener(tableSelectionListener);
		
		dowFileVersionViewerBorder=new TitledBorder(null, "File viewer", TitledBorder.LEADING, TitledBorder.TOP, null, null);
		dowFileVersionViewerPanel.setBorder(dowFileVersionViewerBorder);
		splitPane_1.setRightComponent(dowFileVersionViewerPanel);
		
		JMenuBar menuBar_1 = new JMenuBar();
		frmModExplorer.getContentPane().add(menuBar_1, BorderLayout.NORTH);
		
		JButton btnOpen = new JButton((String) null);
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdMenuOpen();
			}
		});
		
		JButton btnRefresh = new JButton((String) null);
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdRefresh();
			}
		});
		btnRefresh.setIcon(new ImageIcon(ModExplorer.class.getResource("/fr/dow/modexplorer/res/view-refresh-4.png")));
		menuBar_1.add(btnRefresh);
		btnOpen.setToolTipText("Open .module");
		btnOpen.setIcon(new ImageIcon(ModExplorer.class.getResource("/fr/dow/modexplorer/res/document-open-2.png")));
		menuBar_1.add(btnOpen);
		
		JButton btnCreateFile = new JButton((String) null);
		btnCreateFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdActionCreateFiles();
			}
		});
		btnCreateFile.setIcon(new ImageIcon(ModExplorer.class.getResource("/fr/dow/modexplorer/res/document-save-3.png")));
		btnCreateFile.setToolTipText("Create file on disk");
		menuBar_1.add(btnCreateFile);
		
		JButton btnOpenExplorer = new JButton((String) null);
		btnOpenExplorer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdActionOpenExplorer();
			}
		});
		btnOpenExplorer.setToolTipText("Open Explorer Window");
		btnOpenExplorer.setIcon(new ImageIcon(ModExplorer.class.getResource("/fr/dow/modexplorer/res/window-new-5.png")));
		menuBar_1.add(btnOpenExplorer);
		
		JButton btnMassReplace = new JButton((String) null);
		btnMassReplace.setToolTipText("Mass replace string");
		btnMassReplace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdActionMassReplace();
			}
		});
		btnMassReplace.setIcon(new ImageIcon(ModExplorer.class.getResource("/fr/dow/modexplorer/res/edit-find-and-replace-3.png")));
		menuBar_1.add(btnMassReplace);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		menuBar_1.add(horizontalGlue);
		
		btnLowVersion = new JButton("Low Version");
		btnLowVersion.setToolTipText("Show/Hide files inactivated by files from higher mod");
		btnLowVersion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdSwitchDispLowVersion();
			}
		});
		menuBar_1.add(btnLowVersion);
		
		btnLowMod = new JButton("Low Mod");
		btnLowMod.setToolTipText("Show/Hide active files stored in lower mods");
		btnLowMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdSwitchDispLowMod();
			}
		});
		menuBar_1.add(btnLowMod);
		
		btnArchive = new JButton("Archive");
		btnArchive.setToolTipText("Show/Hide files stored in sga archive files");
		btnArchive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdSwitchDispArchive();
			}
		});
		menuBar_1.add(btnArchive);
		
		JPanel panel_1 = new JPanel();
		frmModExplorer.getContentPane().add(panel_1, BorderLayout.SOUTH);
		

	}

}
