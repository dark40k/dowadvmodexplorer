package fr.dow.modexplorer.modexplorerdata;

import fr.dow.gamedata.IniFile;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel;

public class ModExplorerData {

	private static final String iniFileName="./modExplorer.ini";
	
	private IniFile iniFile;

	public RecentModsList recentModsList = new RecentModsList();
	
	public ModExplorerData() {
		System.out.println("Loading : "+iniFileName);
		iniFile=new IniFile(iniFileName);
		recentModsList.importFromIniFile(iniFile);
		DowMod.getDowModParams().importFromIniFile(iniFile);
		DowFileVersionViewerPanel.extensionViewers.importFromIniFile(iniFile);
	}
	
	public void save() {
		recentModsList.exportToIniFile(iniFile);
		DowMod.getDowModParams().exportToIniFile(iniFile);
		DowFileVersionViewerPanel.extensionViewers.exportToIniFile(iniFile);
		iniFile.save();
	}

}
