package fr.dow.modexplorer.modexplorerdata;

import java.io.File;
import java.util.EventListener;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.event.EventListenerList;

import fr.dow.gamedata.IniFile;

public class RecentModsList {

	public static final Integer maxMods=5;
	
	private static final String sectionNameRecentMap="Recent Files";
	private static final String propertyBaseNameRecentMap="recent.";
	
	public Vector<String> recentModsNames=new Vector<String>(maxMods);
	
	public void addFile(File file) {
		String formattedFileName=file.getAbsolutePath().replace('/','\'');
		recentModsNames.remove(formattedFileName);
		recentModsNames.add(0,formattedFileName);
		recentModsNames.setSize(maxMods);
		
		fireRecentMapListUpdateEvent();
	}
	
	//
	// Import / Export to iniFile
	//
	
	public void importFromIniFile(IniFile iniFile) {
		
		recentModsNames.clear();
		
		Integer mapNbr=RecentModsList.maxMods-1;
		do {
			String mapName=iniFile.getStringProperty(sectionNameRecentMap, propertyBaseNameRecentMap+mapNbr);
			if (mapName!=null) 
				if ((new File(mapName)).exists())
					recentModsNames.add(0,mapName);
			mapNbr--;
		} while (mapNbr>=0);
		
		fireRecentMapListUpdateEvent();
	}

	public void exportToIniFile(IniFile iniFile) {
		
		iniFile.addSection(sectionNameRecentMap, null);
		
		for (Integer mapNbr=0; mapNbr<recentModsNames.size();mapNbr++) {
			iniFile.setStringProperty(
					sectionNameRecentMap, 
					propertyBaseNameRecentMap+mapNbr, 
					recentModsNames.get(mapNbr), 
					null);
		}
		
	}

	//
	// Change event
	//
	
	private EventListenerList listenerList = new EventListenerList();

	public class UpdateRecentMapListEvent extends EventObject {
		public UpdateRecentMapListEvent(Object source) {
			super(source);
		}
	}

	public interface UpdateRecentMapListListener extends EventListener {
		public void recentMapListUpdated(UpdateRecentMapListEvent evt);
	}

	public void addUpdateListener(UpdateRecentMapListListener listener) {
		listenerList.add(UpdateRecentMapListListener.class, listener);
	}
	
	public void removeUpdateListener(UpdateRecentMapListListener listener) {
		listenerList.remove(UpdateRecentMapListListener.class, listener);
	}
	
	public void fireRecentMapListUpdateEvent() {
		UpdateRecentMapListEvent evt=new UpdateRecentMapListEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == UpdateRecentMapListListener.class) {
				((UpdateRecentMapListListener) listeners[i+1]).recentMapListUpdated(evt);
			}
		}
	}
	
}
