package fr.dow.modexplorer.gui.parameters;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.util.Map.Entry;

import javax.swing.table.DefaultTableModel;

import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.gamedata.filesystem.DowMod.DowModParams;

import javax.swing.JButton;
import java.awt.Component;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class OpenModPanel extends JPanel {

	private JTable table;

	private DefaultTableModel tableModel;

	/**
	 * Create the panel.
	 */
	public OpenModPanel() {
		setLayout(new BorderLayout(0, 0));
		
		
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Replacements for .module files", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane);
		
		table = new JTable();
		
		tableModel = new DefaultTableModel
				(null, new String[] { "Search string", "Replace string" } ) 
				{
					Class<?>[] columnTypes = new Class[] {	String.class, String.class };
					public Class<?> getColumnClass(int columnIndex) {
						return columnTypes[columnIndex];
					}
				};

		load(DowMod.getDowModParams());
		
		table.setModel(tableModel);
		
		table.getColumnModel().getColumn(1).setPreferredWidth(117);
		
		scrollPane.setViewportView(table);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		add(panel_1, BorderLayout.EAST);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{107, 0};
		gbl_panel_1.rowHeights = new int[]{23, 23, 23, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnNewButton = new JButton("Insert line");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdAddLine();
			}
		});
		btnNewButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 0;
		panel_1.add(btnNewButton, gbc_btnNewButton);
		
		JButton btnDeleteLine = new JButton("Delete line");
		btnDeleteLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdDeleteLine();
			}
		});
		btnDeleteLine.setAlignmentX(Component.CENTER_ALIGNMENT);
		GridBagConstraints gbc_btnDeleteLine = new GridBagConstraints();
		gbc_btnDeleteLine.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDeleteLine.insets = new Insets(0, 0, 5, 0);
		gbc_btnDeleteLine.gridx = 0;
		gbc_btnDeleteLine.gridy = 1;
		panel_1.add(btnDeleteLine, gbc_btnDeleteLine);
		
		JButton btnDefault = new JButton("Restore default");
		btnDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdRestoreDefault();
			}
		});
		btnDefault.setAlignmentX(0.5f);
		GridBagConstraints gbc_btnDefault = new GridBagConstraints();
		gbc_btnDefault.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDefault.gridx = 0;
		gbc_btnDefault.gridy = 2;
		panel_1.add(btnDefault, gbc_btnDefault);

	}

	
	private void load(DowModParams modParams) {
		
		while (tableModel.getRowCount()>0)
			tableModel.removeRow(0);
		
		for (Entry<String,String> entry : modParams.getSearchReplaceKeys().entrySet() ) {
			tableModel.addRow(new String[] { entry.getKey(), entry.getValue() });
		}
		
		tableModel.fireTableDataChanged();
	}

	
	public void save() {

		DowMod.getDowModParams().clear();

		for (int i = 0; i < tableModel.getRowCount(); i++) {
			DowMod.getDowModParams().addSearchReplaceKey(
					(String) tableModel.getValueAt(i, 0),
					(String) tableModel.getValueAt(i, 1));
		}

	}

	public void cmdAddLine() {
		if (table.getSelectedRow()<0) {
			tableModel.addRow(new String[] { "", "" });
			return;
		}
		tableModel.insertRow(table.getSelectedRow(), new String[] { "", "" });
		tableModel.fireTableDataChanged();
	}

	public void cmdDeleteLine() {
		if (table.getSelectedRow()<0) return;
		tableModel.removeRow(table.getSelectedRow());
		tableModel.fireTableDataChanged();
	}

	public void cmdRestoreDefault() {
		DowModParams modParams = new DowModParams();
		load(modParams);
	}

}
