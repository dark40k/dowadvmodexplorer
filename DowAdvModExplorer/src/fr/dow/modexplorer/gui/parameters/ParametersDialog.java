package fr.dow.modexplorer.gui.parameters;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import fr.dow.modexplorer.modexplorerdata.ModExplorerData;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;

public class ParametersDialog extends JDialog {
	
	@SuppressWarnings("unused")
	private ModExplorerData data;
	
	private OpenModPanel openModPanel;

	private ViewerFormatsPanel viewerFormatsPanel;
	
	private void cmdOK() {
		openModPanel.save();
		viewerFormatsPanel.save();
		
		this.setVisible(false);
	}
	
	private void cmdCancel() {
		this.setVisible(false);
	}

	public static void showDialog(Window owner, ModExplorerData data) {
		try {
			ParametersDialog dialog = new ParametersDialog(owner, data);
			dialog.setVisible(true);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ParametersDialog(Window owner, ModExplorerData data) {
		
		super(owner);
		setTitle("Parameters");
		this.data=data;
		
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		initialize();
	}

	private void initialize() {
		
		setBounds(100, 100, 742, 616);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cmdOK();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cmdCancel();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		{
			JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			getContentPane().add(tabbedPane, BorderLayout.CENTER);
			
			openModPanel = new OpenModPanel();
			tabbedPane.addTab("Load mod parameters", null, openModPanel, null);
			
			viewerFormatsPanel = new ViewerFormatsPanel();
			tabbedPane.addTab("Viewer formats", null, viewerFormatsPanel, null);

		}
		
		
	}
	
}
