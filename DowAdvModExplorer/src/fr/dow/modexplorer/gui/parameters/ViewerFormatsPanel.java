package fr.dow.modexplorer.gui.parameters;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.util.Map.Entry;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel;
import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel.ExtensionViewerType;
import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel.ViewerType;

import javax.swing.JButton;
import java.awt.Component;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class ViewerFormatsPanel extends JPanel {

	private JTable table;

	private DefaultTableModel tableModel;

	/**
	 * Create the panel.
	 */
	public ViewerFormatsPanel() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Association table", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane);
		
		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
	    tableModel = (DefaultTableModel) table.getModel();
	    tableModel.addColumn("Extension", new Object[] {});
	    tableModel.addColumn("Viewer", new Object[] {});
	    
		scrollPane.setViewportView(table);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		add(panel_1, BorderLayout.EAST);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{107, 0};
		gbl_panel_1.rowHeights = new int[]{23, 23, 23, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnNewButton = new JButton("Insert line");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdAddLine();
			}
		});
		btnNewButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 0;
		panel_1.add(btnNewButton, gbc_btnNewButton);
		
		JButton btnDeleteLine = new JButton("Delete line");
		btnDeleteLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdDeleteLine();
			}
		});
		btnDeleteLine.setAlignmentX(Component.CENTER_ALIGNMENT);
		GridBagConstraints gbc_btnDeleteLine = new GridBagConstraints();
		gbc_btnDeleteLine.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDeleteLine.insets = new Insets(0, 0, 5, 0);
		gbc_btnDeleteLine.gridx = 0;
		gbc_btnDeleteLine.gridy = 1;
		panel_1.add(btnDeleteLine, gbc_btnDeleteLine);
		
		JButton btnDefault = new JButton("Restore default");
		btnDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdRestoreDefault();
			}
		});
		btnDefault.setAlignmentX(0.5f);
		GridBagConstraints gbc_btnDefault = new GridBagConstraints();
		gbc_btnDefault.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDefault.gridx = 0;
		gbc_btnDefault.gridy = 2;
		panel_1.add(btnDefault, gbc_btnDefault);

		init(DowFileVersionViewerPanel.extensionViewers);
		
	}

	public void init(DowFileVersionViewerPanel.ExtensionViewerType extViewer) {
		
	    // get viewer types
	    ViewerType[] listViewerTypes = DowFileVersionViewerPanel.ViewerType.values();
	    String[] listNames = new String[listViewerTypes.length];
	    for ( int i=0; i<listViewerTypes.length; i++) listNames[i]=listViewerTypes[i].getName();
	    
	    TableColumn col = table.getColumnModel().getColumn(1);
	    col.setCellEditor(new MyComboBoxEditor(listNames));
	    col.setCellRenderer(new MyComboBoxRenderer(listNames));
		
	    // init table
		while (tableModel.getRowCount()>0)
			tableModel.removeRow(0);
		
		for (Entry<String, ViewerType> entry : extViewer.extensionTable.entrySet() ) {
			tableModel.addRow(new String[] { entry.getKey(), entry.getValue().getName() });
		}
		
		tableModel.fireTableDataChanged();
	    
	}
	
	public void save() {
		
		DowFileVersionViewerPanel.extensionViewers.extensionTable.clear();

		for (int i = 0; i < tableModel.getRowCount(); i++)
			DowFileVersionViewerPanel.extensionViewers.extensionTable.put(
					(String) tableModel.getValueAt(i, 0),
					DowFileVersionViewerPanel.ViewerType.getByName((String) tableModel.getValueAt(i, 1)));
		
	}

	public void cmdAddLine() {
		if (table.getSelectedRow()<0) {
			tableModel.addRow(new String[] { "", "" });
			return;
		}
		tableModel.insertRow(table.getSelectedRow(), new String[] { "", "" });
		tableModel.fireTableDataChanged();
	}

	public void cmdDeleteLine() {
		if (table.getSelectedRow()<0) return;
		tableModel.removeRow(table.getSelectedRow());
		tableModel.fireTableDataChanged();
	}

	public void cmdRestoreDefault() {
		ExtensionViewerType viewerType = new DowFileVersionViewerPanel.ExtensionViewerType();
		viewerType.resetDefault();
		init(viewerType);
	}

	//
	// ComboBox for cell editing
	//
	
	class MyComboBoxRenderer extends JComboBox<String> implements TableCellRenderer {

		public MyComboBoxRenderer(String[] items) {
			super(items);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			if (isSelected) {
				setForeground(table.getSelectionForeground());
				super.setBackground(table.getSelectionBackground());
			} else {
				setForeground(table.getForeground());
				setBackground(table.getBackground());
			}
			setSelectedItem(value);
			return this;
		}
	}

	class MyComboBoxEditor extends DefaultCellEditor {
		public MyComboBoxEditor(String[] items) {
			super(new JComboBox<String>(items));
		}
	}

}
