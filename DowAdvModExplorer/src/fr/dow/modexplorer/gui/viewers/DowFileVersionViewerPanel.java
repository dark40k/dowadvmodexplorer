package fr.dow.modexplorer.gui.viewers;

import javax.swing.JPanel;

import fr.dow.gamedata.IniFile;
import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.modexplorer.gui.viewers.picture2.Picture2Viewer;
import java.awt.BorderLayout;
import java.util.HashMap;
import java.util.Map.Entry;

public class DowFileVersionViewerPanel extends JPanel {

	private static String sectionNameViewerFormats = "ViewerFormats";
	private static String propertyExtensionKey = "Extension.";
	private static String propertyViewerString = "Viewer.";
	
	public interface DowFileViewer {
		public void setDowFileVersion(DowFileVersion dowFileVersion);
		public JPanel getPanel();
	}

	public enum ViewerType {
		NOVIEWER("No Viewer", new DefaultViewer()),
		HEXVIEWER("Hex viewer", new HexFileViewer()),		
		TEXT("Text", new TextViewer()),
		CHUNKFILE("Chunk File", new ChunkFileViewer()),
		PICTURE("Picture", new PictureViewer()),
		PICTUREOPENGL("PictureOpenGL", new Picture2Viewer());
		
		private final String name;
		private final DowFileViewer viewer;

		ViewerType(String name, DowFileViewer viewer)  {
			this.name=name;
			this.viewer=viewer;
		}
		
		public String getName() {
			return name;
		}
		
		public DowFileViewer getViewer()  {
			return viewer;
		}
		
		public static ViewerType getDefaultViewer() {
			return HEXVIEWER;
		}
		
		public static ViewerType getByName(String viewerName) {
			for (ViewerType type : ViewerType.values()) 
				if (viewerName.equals(type.getName()))
					return type;
			return ViewerType.getDefaultViewer();
		}
	}
	
	public final static ExtensionViewerType extensionViewers = new ExtensionViewerType();
	
	private ViewerType viewerType;
	
	//
	// Constructor
	//
	public DowFileVersionViewerPanel() {
		setLayout(new BorderLayout(0, 0));
		
	}

	//
	// Update file to show
	//

	public void setDowFileVersion(DowFileVersion dowFileVersion) {
		
		if (viewerType!=null) {
			viewerType.getViewer().setDowFileVersion(null);
			this.remove(viewerType.getViewer().getPanel());
		}
				
		if (dowFileVersion==null) 
			viewerType = ViewerType.NOVIEWER;
		else
			viewerType = extensionViewers.getViewerType(dowFileVersion.getDowFile().getName());
		
		add(viewerType.getViewer().getPanel(), BorderLayout.CENTER);
		viewerType.getViewer().setDowFileVersion(dowFileVersion);

		revalidate();
		repaint();
		
		
	}
	
	//
	// Class for storing parameters
	//
	
	public static class ExtensionViewerType {

		public final HashMap<String, ViewerType> extensionTable = new HashMap<String, ViewerType>();

		public ViewerType getViewerType(String filename) {
			int pos = filename.lastIndexOf(".");
			if (pos<0) return ViewerType.getDefaultViewer();
			String extension = filename.substring(pos+1);
			ViewerType viewer = extensionTable.get(extension);
			if (viewer==null) return ViewerType.getDefaultViewer();
			return viewer;
		}
				
		//
		// Default values
		//
		
		public void resetDefault() {
			extensionTable.clear();
			extensionTable.put("ai", ViewerType.TEXT);
			extensionTable.put("rat", ViewerType.TEXT);
			extensionTable.put("module", ViewerType.TEXT);
			extensionTable.put("lua", ViewerType.TEXT);
			extensionTable.put("scar", ViewerType.TEXT);
			extensionTable.put("teamcolour", ViewerType.TEXT);
			extensionTable.put("jpg", ViewerType.PICTURE);
			extensionTable.put("jpeg", ViewerType.PICTURE);
			extensionTable.put("png", ViewerType.PICTURE);
			extensionTable.put("bmp", ViewerType.PICTURE);
			extensionTable.put("wbmp", ViewerType.PICTURE);
			extensionTable.put("gif", ViewerType.PICTURE);
			extensionTable.put("sgm", ViewerType.CHUNKFILE);
			extensionTable.put("fda", ViewerType.CHUNKFILE);
			extensionTable.put("sga", ViewerType.CHUNKFILE);
			extensionTable.put("sgb", ViewerType.CHUNKFILE);
			extensionTable.put("whe", ViewerType.CHUNKFILE);
			extensionTable.put("whm", ViewerType.CHUNKFILE);
			extensionTable.put("events", ViewerType.CHUNKFILE);
			extensionTable.put("rsh", ViewerType.PICTUREOPENGL);
			extensionTable.put("tga", ViewerType.PICTUREOPENGL);
			extensionTable.put("dds", ViewerType.PICTUREOPENGL);
		}
		
		//
		// Import / Export functions
		//

		public void importFromIniFile(IniFile iniFile) {

			if (!iniFile.containsSection(sectionNameViewerFormats)) {
				resetDefault();
				return;
			}

			extensionTable.clear();

			Integer lineNbr = 0;
			do {
				String searchKey = iniFile.getStringProperty(sectionNameViewerFormats, propertyExtensionKey + lineNbr);
				String replaceString = iniFile.getStringProperty(sectionNameViewerFormats, propertyViewerString + lineNbr);

				if ((searchKey != null) && (replaceString != null))
					extensionTable.put(searchKey, ViewerType.getByName(replaceString));
				else
					break;

				lineNbr++;

			} while (true);

		}

		public void exportToIniFile(IniFile iniFile) {

			iniFile.addSection(sectionNameViewerFormats, null);

			Integer lineNbr = 0;
			for (Entry<String, ViewerType> entry : extensionTable.entrySet()) {
				iniFile.setStringProperty(sectionNameViewerFormats, propertyExtensionKey + lineNbr, entry.getKey(), null);
				iniFile.setStringProperty(sectionNameViewerFormats, propertyViewerString + lineNbr, entry.getValue().getName(), null);
				lineNbr++;
			}

		}

	}
	
}
