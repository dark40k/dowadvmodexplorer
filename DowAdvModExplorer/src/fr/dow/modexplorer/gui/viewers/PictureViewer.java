package fr.dow.modexplorer.gui.viewers;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel.DowFileViewer;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

public class PictureViewer implements DowFileViewer {
	
	private final JPanel panel = new JPanel();
	
	private final JLabel picLabel;
	private final JLabel picDataLabel;
	
	public PictureViewer() {
		
		picLabel = new JLabel("<Empty>");
		picLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel subPanel = new JPanel();
		subPanel.setLayout(new BorderLayout(0, 0));
		subPanel.add(picLabel, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(subPanel);
		
		picDataLabel = new JLabel("<Empty>");
		subPanel.add(picDataLabel, BorderLayout.NORTH);
		
		panel.setLayout(new BorderLayout(0, 0));
		panel.add(scrollPane, BorderLayout.CENTER);
		
	}

	@Override
	public void setDowFileVersion(DowFileVersion dowFileVersion) {
		
		if (dowFileVersion==null) {
			picLabel.setIcon(null);
			picLabel.setText("<No picture>");
			return;
		}
		
		try {
			BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(dowFileVersion.getData()));

			if (bufferedImage==null) {
				picDataLabel.setText("Failed to load picture");
				picLabel.setIcon(null);
				return;
			}
			
			picDataLabel.setText(bufferedImage.getWidth() + " x " + bufferedImage.getHeight());
			picLabel.setText(null);
			picLabel.setIcon(new ImageIcon(bufferedImage));

		} catch (IOException e) {
			picLabel.setIcon(null);
			picDataLabel.setText(e.getMessage());
			e.printStackTrace();
		}

	}

	@Override
	public JPanel getPanel() {
		return panel;
	}

};
