package fr.dow.modexplorer.gui.viewers;

import java.awt.BorderLayout;
import java.io.IOException;

import javax.swing.JPanel;

import fr.dow.gamedata.RelicChunkFile;
import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel.DowFileViewer;
import fr.dow.modexplorer.gui.viewers.chunkfile.ChunkViewer;
import fr.dow.modexplorer.gui.viewers.chunkfile.TreeChunk;
import fr.dow.modexplorer.gui.viewers.chunkfile.TreeChunkNode;

import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

public class ChunkFileViewer implements DowFileViewer {
	
	private final JPanel panel = new JPanel();
	
	private final TreeChunk treeChunk = new TreeChunk();
	private final ChunkViewer chunkViewer = new ChunkViewer();
	
	
	public ChunkFileViewer() {
		
		treeChunk.addTreeSelectionListener(treeListener);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(treeChunk);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setLeftComponent(scrollPane);
		splitPane.setRightComponent(chunkViewer);

		panel.setLayout(new BorderLayout(0, 0));
		panel.add(splitPane, BorderLayout.CENTER);
	}

	@Override
	public void setDowFileVersion(DowFileVersion dowFileVersion) {
		
		if (dowFileVersion==null) {
			treeChunk.setChunkFile("",null);
			return;
		}
		
		try {
			treeChunk.setChunkFile(dowFileVersion.getDowFile().getName(), new RelicChunkFile(dowFileVersion));
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public JPanel getPanel() {
		return panel;
	}

	private final TreeSelectionListener treeListener = new TreeSelectionListener() {
		@Override
		public void valueChanged(TreeSelectionEvent e) {
			TreeChunkNode chunkNode = treeChunk.getLastSelectedNode();
			if (chunkNode != null) chunkViewer.setChunk(chunkNode.getChunk());
			else chunkViewer.setChunk(null);
		}
	};

};
