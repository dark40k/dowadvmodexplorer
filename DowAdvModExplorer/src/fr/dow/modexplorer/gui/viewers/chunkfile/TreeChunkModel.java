package fr.dow.modexplorer.gui.viewers.chunkfile;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import fr.dow.gamedata.RelicChunkFile;

import java.util.Vector;

public class TreeChunkModel implements TreeModel {

	private Vector<TreeModelListener> treeModelListeners = new Vector<TreeModelListener>();

	private TreeChunkNode rootChunkNode;

	public TreeChunkModel() {}

	/*
	 * public TreeChunkModel(String baseName, RelicChunkFile root) {
	 * rootChunkNode = new TreeChunkNode(baseName,root); }
	 */

	public void setNewRootChunkFile(String baseName, RelicChunkFile newRoot) {
		
		TreeChunkNode oldRoot = rootChunkNode;

		if (newRoot != null)
			rootChunkNode = new TreeChunkNode(baseName, newRoot);
		else 
			rootChunkNode = null;

		if (oldRoot != null) fireTreeStructureChanged(oldRoot);
	}

	public void forceUpdate() {
		if (rootChunkNode != null) {
			fireTreeStructureChanged(rootChunkNode);
		}
	}
	
	public void forceCompleteUpdate() {
		if (rootChunkNode != null) {
			rootChunkNode.reloadChunkFile();
			fireTreeStructureChanged(rootChunkNode);
		}
	}


	// ////////////// Fire events //////////////////////////////////////////////

	/**
	 * The only event raised by this model is TreeStructureChanged with the root
	 * as path, i.e. the whole tree has changed.
	 */
	protected void fireTreeStructureChanged(TreeChunkNode oldRoot) {
		// int len = treeModelListeners.size();
		TreeModelEvent e = new TreeModelEvent(this, new Object[] { oldRoot });
		for (TreeModelListener tml : treeModelListeners)
			tml.treeStructureChanged(e);
	}

	// ////////////// TreeModel interface implementation ///////////////////////

	/**
	 * Adds a listener for the TreeModelEvent posted after the tree changes.
	 */
	public void addTreeModelListener(TreeModelListener l) {
		treeModelListeners.addElement(l);
	}

	/**
	 * Returns the child of parent at index index in the parent's child array.
	 */
	public Object getChild(Object node, int index) {
		TreeChunkNode chunkNode = (TreeChunkNode) node;
		return chunkNode.getChildAt(index);
	}

	/**
	 * Returns the number of children of parent.
	 */
	public int getChildCount(Object node) {
		TreeChunkNode chunkNode = (TreeChunkNode) node;
		return chunkNode.getChildCount();
	}

	/**
	 * Returns the index of child in parent.
	 */
	public int getIndexOfChild(Object parent, Object child) {
		TreeChunkNode chunkNodeParent = (TreeChunkNode) parent;
		TreeChunkNode chunkNodeChild = (TreeChunkNode) child;
		return chunkNodeParent.getIndexOfChild(chunkNodeChild);
	}

	/**
	 * Returns the root of the tree.
	 */
	public Object getRoot() {
		return rootChunkNode;
	}

	/**
	 * Returns true if node is a leaf.
	 */
	public boolean isLeaf(Object node) {
		TreeChunkNode chunkNode = (TreeChunkNode) node;
		return chunkNode.isDATA();
	}

	/**
	 * Removes a listener previously added with addTreeModelListener().
	 */
	public void removeTreeModelListener(TreeModelListener l) {
		treeModelListeners.removeElement(l);
	}

	/**
	 * Messaged when the user has altered the value for the item identified by
	 * path to newValue. Not used by this model.
	 */
	public void valueForPathChanged(TreePath path, Object newValue) {
		System.out.println("*** valueForPathChanged : " + path + " --> " + newValue);
	}

	public void setBaseName(String baseName) {
		rootChunkNode.setName(baseName);
		fireTreeStructureChanged(rootChunkNode);
	}

	public TreeChunkNode findNextNodeID(String searchString, TreeChunkNode startNode, boolean ignoreCase) {

		if (ignoreCase) {
			searchString = searchString.toUpperCase();
			while (startNode != null) {
				if (startNode.getChunk().get_id().toUpperCase().contains(searchString)) break;
				startNode = startNode.next();
			}
		} else while (startNode != null) {
			if (startNode.getChunk().get_id().contains(searchString)) break;
			startNode = startNode.next();
		}

		return startNode;
	}

	public TreeChunkNode findNextNodeName(String searchString, TreeChunkNode startNode, boolean ignoreCase) {

		if (ignoreCase) {
			searchString = searchString.toUpperCase();
			while (startNode != null) {
				if (startNode.getChunk().get_name().toUpperCase().contains(searchString)) break;
				startNode = startNode.next();
			}
		} else while (startNode != null) {
			if (startNode.getChunk().get_name().contains(searchString)) break;
			startNode = startNode.next();
		}

		return startNode;
	}

}