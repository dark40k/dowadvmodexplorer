package fr.dow.modexplorer.gui.viewers.chunkfile;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import fr.dow.hexeditor.swing.HexEditor;

import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;


public abstract class ChunkViewerBase extends JPanel {
	
	protected JTextField textFieldType;
	protected JTextField textFieldChunkHeaderSize;
	protected JTextField textFieldID;
	protected JTextField textFieldChunkContentSize;
	protected JTextField textFieldVersion;
	protected JTextField textFieldName;
	protected HexEditor hexEditor;
	protected JPanel panelHexEditor;
	private Component horizontalStrut_1;
	private Component rigidArea;
	private Component rigidArea_1;

	/**
	 * Create the panel.
	 */
	public ChunkViewerBase() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel panelChunkData = new JPanel();
		panelChunkData.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Chunk Header Data", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panelChunkData, BorderLayout.NORTH);
		GridBagLayout gbl_panelChunkData = new GridBagLayout();
		gbl_panelChunkData.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelChunkData.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panelChunkData.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panelChunkData.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelChunkData.setLayout(gbl_panelChunkData);
		
		rigidArea = Box.createRigidArea(new Dimension(10, 10));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea.gridx = 0;
		gbc_rigidArea.gridy = 0;
		panelChunkData.add(rigidArea, gbc_rigidArea);
		
		JLabel label = new JLabel("Type:");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.EAST;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 1;
		panelChunkData.add(label, gbc_label);
		
		textFieldType = new JTextField();
		textFieldType.setEditable(false);
		textFieldType.setColumns(10);
		GridBagConstraints gbc_textFieldType = new GridBagConstraints();
		gbc_textFieldType.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldType.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldType.gridx = 2;
		gbc_textFieldType.gridy = 1;
		panelChunkData.add(textFieldType, gbc_textFieldType);
		
		horizontalStrut_1 = Box.createHorizontalStrut(10);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 3;
		gbc_horizontalStrut_1.gridy = 1;
		panelChunkData.add(horizontalStrut_1, gbc_horizontalStrut_1);
		
		JLabel label_1 = new JLabel("Chunk Header Size:");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.WEST;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 4;
		gbc_label_1.gridy = 1;
		panelChunkData.add(label_1, gbc_label_1);
		
		textFieldChunkHeaderSize = new JTextField();
		textFieldChunkHeaderSize.setEditable(false);
		textFieldChunkHeaderSize.setColumns(10);
		GridBagConstraints gbc_textFieldChunkHeaderSize = new GridBagConstraints();
		gbc_textFieldChunkHeaderSize.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldChunkHeaderSize.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldChunkHeaderSize.gridx = 5;
		gbc_textFieldChunkHeaderSize.gridy = 1;
		panelChunkData.add(textFieldChunkHeaderSize, gbc_textFieldChunkHeaderSize);
		
		JLabel label_2 = new JLabel("ID:");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.anchor = GridBagConstraints.EAST;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 1;
		gbc_label_2.gridy = 2;
		panelChunkData.add(label_2, gbc_label_2);
		
		textFieldID = new JTextField();
		textFieldID.setEditable(false);
		textFieldID.setColumns(10);
		GridBagConstraints gbc_textFieldID = new GridBagConstraints();
		gbc_textFieldID.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldID.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldID.gridx = 2;
		gbc_textFieldID.gridy = 2;
		panelChunkData.add(textFieldID, gbc_textFieldID);
		
		JLabel label_3 = new JLabel("Chunk Contents Size:");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.anchor = GridBagConstraints.WEST;
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 4;
		gbc_label_3.gridy = 2;
		panelChunkData.add(label_3, gbc_label_3);
		
		textFieldChunkContentSize = new JTextField();
		textFieldChunkContentSize.setEditable(false);
		textFieldChunkContentSize.setColumns(10);
		GridBagConstraints gbc_textFieldChunkContentSize = new GridBagConstraints();
		gbc_textFieldChunkContentSize.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldChunkContentSize.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldChunkContentSize.gridx = 5;
		gbc_textFieldChunkContentSize.gridy = 2;
		panelChunkData.add(textFieldChunkContentSize, gbc_textFieldChunkContentSize);
		
		JLabel label_4 = new JLabel("Version:");
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.anchor = GridBagConstraints.WEST;
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 1;
		gbc_label_4.gridy = 3;
		panelChunkData.add(label_4, gbc_label_4);
		
		textFieldVersion = new JTextField();
		textFieldVersion.setEditable(false);
		textFieldVersion.setColumns(10);
		GridBagConstraints gbc_textFieldVersion = new GridBagConstraints();
		gbc_textFieldVersion.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldVersion.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldVersion.gridx = 2;
		gbc_textFieldVersion.gridy = 3;
		panelChunkData.add(textFieldVersion, gbc_textFieldVersion);
		
		JLabel label_6 = new JLabel("Name:");
		label_6.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.anchor = GridBagConstraints.EAST;
		gbc_label_6.insets = new Insets(0, 0, 5, 5);
		gbc_label_6.gridx = 1;
		gbc_label_6.gridy = 4;
		panelChunkData.add(label_6, gbc_label_6);
		
		textFieldName = new JTextField();
		textFieldName.setEditable(false);
		textFieldName.setColumns(10);
		GridBagConstraints gbc_textFieldName = new GridBagConstraints();
		gbc_textFieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldName.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldName.gridx = 2;
		gbc_textFieldName.gridy = 4;
		panelChunkData.add(textFieldName, gbc_textFieldName);
		
		rigidArea_1 = Box.createRigidArea(new Dimension(10, 10));
		GridBagConstraints gbc_rigidArea_1 = new GridBagConstraints();
		gbc_rigidArea_1.gridx = 6;
		gbc_rigidArea_1.gridy = 5;
		panelChunkData.add(rigidArea_1, gbc_rigidArea_1);
		
		panelHexEditor = new JPanel();
		add(panelHexEditor);
		panelHexEditor.setLayout(new BorderLayout(0, 0));
		
		hexEditor = new HexEditor();
		panelHexEditor.add(hexEditor, BorderLayout.CENTER);

	}
	
}
