package fr.dow.modexplorer.gui.viewers.chunkfile;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;

public class ChunkViewer extends ChunkViewerBase {

	RelicChunk chunk;

	public ChunkViewer() {
		super();
		panelHexEditor.setVisible(false);
	}

	public void setChunk(RelicChunk newChunk) {

		chunk = newChunk;
		
		if (newChunk == null) {
			clear();
			panelHexEditor.setVisible(false);
			return;
		}

		panelHexEditor.setVisible(chunk.get_type().equals(RelicChunkDATA.type));
		
		updateChunkData();
		
	}

	public void updateChunkData() {

		if (chunk == null) return;
		
		textFieldType.setText(chunk.get_type());
		textFieldChunkHeaderSize.setText(chunk.get_headersize().toString());
		textFieldID.setText(chunk.get_id());
		textFieldChunkContentSize.setText(chunk.get_datasize().toString());
		textFieldVersion.setText(chunk.get_version().toString());
		textFieldName.setText(chunk.get_name());
		
		if (chunk.get_type().equals(RelicChunkDATA.type)) {
			try {
				hexEditor.open(new ByteArrayInputStream(((RelicChunkDATA) chunk).dataArray()));
			}
			catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public RelicChunk getChunk() {
		return chunk;
	}
				
	public void clear() {

		chunk = null;

		textFieldType.setText(null);
		textFieldChunkHeaderSize.setText(null);
		textFieldID.setText(null);
		textFieldChunkContentSize.setText(null);
		textFieldVersion.setText(null);
		textFieldName.setText(null);

	}


}
