package fr.dow.modexplorer.gui.viewers;

import java.awt.BorderLayout;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.swing.JPanel;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.hexeditor.swing.HexEditor;
import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel.DowFileViewer;

public class HexFileViewer implements DowFileViewer {
	
	private final JPanel panel = new JPanel();
	private HexEditor hexEditor;
	
	public HexFileViewer() {

		panel.setLayout(new BorderLayout(0, 0));
		
		hexEditor = new HexEditor();
		panel.add(hexEditor, BorderLayout.CENTER);
		
	}

	@Override
	public void setDowFileVersion(DowFileVersion dowFileVersion) {
		
		if (dowFileVersion==null) {
			try {
				hexEditor.open(new ByteArrayInputStream(new byte[] {}));
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}

		
		try {
			hexEditor.open(new ByteArrayInputStream(dowFileVersion.getData()));
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public JPanel getPanel() {
		return panel;
	}

};
