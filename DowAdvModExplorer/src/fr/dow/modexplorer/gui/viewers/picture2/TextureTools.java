package fr.dow.modexplorer.gui.viewers.picture2;

import javax.media.opengl.GL2;

public class TextureTools {

	public static int getCurrentTextureFormat(GL2 gl, int level) {
	    int[] tmp = new int[1];
	    gl.glGetTexLevelParameteriv(GL2.GL_TEXTURE_2D, level, GL2.GL_TEXTURE_INTERNAL_FORMAT, tmp, 0);
	    return tmp[0];
	}
	
	public static int getCurrentTextureHeight(GL2 gl, int level) {
	    int[] tmp = new int[1];
	    gl.glGetTexLevelParameteriv(GL2.GL_TEXTURE_2D, level, GL2.GL_TEXTURE_HEIGHT, tmp, 0);
	    return tmp[0];
	}
	
	public static int getCurrentTextureWidth(GL2 gl, int level) {
	    int[] tmp = new int[1];
	    gl.glGetTexLevelParameteriv(GL2.GL_TEXTURE_2D, level, GL2.GL_TEXTURE_WIDTH, tmp, 0);
	    return tmp[0];
	}
	
}
