package fr.dow.modexplorer.gui.viewers.picture2;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import fr.dow.gamedata.filesystem.DowFileVersion;

public class TgaPicture {
	
	public static Texture load(DowFileVersion dowFileVersion) {

		try {
			
			byte[] textureRawDataTGA=dowFileVersion.getData();
			
			if (textureRawDataTGA==null) return null;
			
			Texture newTex=TextureIO.newTexture(new ByteArrayInputStream(textureRawDataTGA), true,TextureIO.TGA);
			return newTex;
			
		} catch (IOException e) {
			System.out.println("Warning : error for " + dowFileVersion.getDowFile().getDowPath());
			e.printStackTrace();
		}
		
		return null;
	}

//	public static int loadID(String texFilename, DowMod modData) {
//		Texture newTex=load(texFilename, modData);
//		if (newTex!=null) return newTex.getTextureObject();
//		return 0;
//	}
	
	public static int loadDirect(String texFilename) {

		try {
			
			File tmpFile=new File(texFilename);
			FileInputStream streamFile=new FileInputStream(tmpFile);
			byte[] textureRawDataTGA=new byte[(int) tmpFile.length()];
			streamFile.read(textureRawDataTGA);
			streamFile.close();
			
			Texture newTex=TextureIO.newTexture(new ByteArrayInputStream(textureRawDataTGA), true,TextureIO.TGA);
			return newTex.getTextureObject();
			
		} catch (IOException e) {
			System.out.println("Warning : error for " + texFilename);
			e.printStackTrace();
		}
		
		return 0;
	}

}
