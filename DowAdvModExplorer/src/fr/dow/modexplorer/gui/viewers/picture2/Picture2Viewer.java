package fr.dow.modexplorer.gui.viewers.picture2;

import java.awt.BorderLayout;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.dow.gamedata.filesystem.DowFileVersion;

import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel.DowFileViewer;

import javax.swing.JScrollPane;
import com.jogamp.opengl.util.FPSAnimator;

public class Picture2Viewer implements DowFileViewer {
	
	private final JPanel panel = new JPanel();
	
	private FPSAnimator animator;

	private PictureRenderer pictureRenderer;

	private JLabel picDataLabel;
	
	public Picture2Viewer() {

		JPanel subPanel = new JPanel();
		subPanel.setLayout(new BorderLayout(0, 0));
		
		picDataLabel = new JLabel("<Empty>");
		subPanel.add(picDataLabel, BorderLayout.NORTH);

		GLCanvas canvas = new GLCanvas();
		pictureRenderer=new PictureRenderer(picDataLabel);
	    canvas.addGLEventListener(pictureRenderer);
	    
		animator = new FPSAnimator( canvas, 25, true);
		animator.start(); 
		animator.pause();
		
		subPanel.add(canvas, BorderLayout.CENTER);
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(subPanel);
		
		panel.setLayout(new BorderLayout(0, 0));
		panel.add(scrollPane, BorderLayout.CENTER);
		
	}

	@Override
	public void setDowFileVersion(DowFileVersion dowFileVersion) {
		animator.pause(); 
		pictureRenderer.displayNewTexure(dowFileVersion);
		if (dowFileVersion!=null) animator.resume(); 
	}

	@Override
	public JPanel getPanel() {
		return panel;
	}

};
