package fr.dow.modexplorer.gui.viewers;

import java.awt.BorderLayout;
import java.io.IOException;

import javax.swing.JPanel;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel.DowFileViewer;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

public class TextViewer implements DowFileViewer {
	
	private final JPanel panel = new JPanel();
	private final JEditorPane editorPane = new JEditorPane();
	
	public TextViewer() {
		
		editorPane.setText("<EMPTY>");
		editorPane.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(editorPane);
		
		panel.setLayout(new BorderLayout(0, 0));
		panel.add(scrollPane, BorderLayout.CENTER);
	}

	@Override
	public void setDowFileVersion(DowFileVersion dowFileVersion) {
		
		if (dowFileVersion==null) {
			editorPane.setText(null);
			return;
		}
		
		try {
			editorPane.setText(new String(dowFileVersion.getData()));
		} catch (IOException e) {
			editorPane.setText(e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public JPanel getPanel() {
		return panel;
	}

};
