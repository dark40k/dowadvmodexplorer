package fr.dow.modexplorer.gui.viewers;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.modexplorer.gui.viewers.DowFileVersionViewerPanel.DowFileViewer;

public class DefaultViewer implements DowFileViewer {
	
	private final JPanel panel = new JPanel();
	private final JLabel noFileLabel = new JLabel("<No file>");
	
	public DefaultViewer() {
		panel.setLayout(new BorderLayout(0, 0));
		panel.add(noFileLabel, BorderLayout.CENTER);
	}

	@Override
	public void setDowFileVersion(DowFileVersion dowFileVersion) {
		if (dowFileVersion==null) {
			noFileLabel.setText("<No file selected>");
			return;
		}
		noFileLabel.setText("No viewer for "+dowFileVersion.getDowFile().getDowPath());
	}

	@Override
	public JPanel getPanel() {
		return panel;
	}

};
