package fr.dow.modexplorer.gui.modtree;

import java.util.Vector;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import fr.dow.gamedata.filesystem.DowMod;

public class ModTreeModel implements TreeModel {

	private Vector<TreeModelListener> treeModelListeners = new Vector<TreeModelListener>();
	
	private ModTreeNode rootNode;
	
	//
	// Initialize treemodel
	// 
	
	public void setNewRootChunkFile(String baseName, DowMod dowMod) {
		
		rootNode = new ModTreeNode(dowMod.getTopFolder(), baseName);
		fireTreeStructureChanged();
	}
	
	//
	// TreeModel implementation
	//
	
	@Override
	public Object getRoot() {
		return rootNode;
	}

	@Override
	public Object getChild(Object parent, int index) {
		ModTreeNode node = (ModTreeNode) parent;
		return node.getChild(index);
	}

	@Override
	public int getChildCount(Object parent) {
		ModTreeNode node = (ModTreeNode) parent;
		return node.getChildCount();
	}

	@Override
	public boolean isLeaf(Object node) {
		return getChildCount(node)==0;
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
		// No edition, never used
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		if ((parent==null) || (child == null)) return -1;
		
		ModTreeNode parentFolder = (ModTreeNode) parent;
		ModTreeNode childFolder = (ModTreeNode) child;
		
		return parentFolder.getIndexOfChild(childFolder);
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		treeModelListeners.addElement(l);
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		treeModelListeners.remove(l);
		
	}

	// ////////////// Fire events //////////////////////////////////////////////

	/**
	 * The only event raised by this model is TreeStructureChanged with the root
	 * as path, i.e. the whole tree has changed.
	 */
	protected void fireTreeStructureChanged() {
		// int len = treeModelListeners.size();
		TreeModelEvent e = new TreeModelEvent(this, new Object[] { getRoot() });
		for (TreeModelListener tml : treeModelListeners)
			tml.treeStructureChanged(e);
	}


}
