package fr.dow.modexplorer.gui.modtree;

import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;

public class ModTree {
	
	private final ModTreeModel treeChunkModel=new ModTreeModel();

	private final JTree tree;

	private DowMod dowMod;
	
	public ModTree() {
		
	    DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
	    renderer.setLeafIcon(renderer.getOpenIcon());
		
		tree = new JTree();
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setExpandsSelectedPaths(true);
        tree.setCellRenderer(renderer);
		
		setMod("<empty>", null);
		
	}

	public JTree getTree() {
		return tree;
	}
	
	public void setMod(String baseName, DowMod dowMod) {

		this.dowMod=dowMod;
		
		if (dowMod==null) {
			tree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode(baseName)));
			tree.setEnabled(false);
			return;
		}
		
		treeChunkModel.setNewRootChunkFile(baseName, dowMod);
		tree.setModel(treeChunkModel);
		tree.getSelectionModel().clearSelection();
		tree.setEnabled(true);

	}
	
	public void addTreeSelectionListener(TreeSelectionListener listener) {
		tree.addTreeSelectionListener(listener);
	}

	public void addMouseListener(MouseListener listener) {
		tree.addMouseListener(listener);
	}
	
	private ModTreeNode getLastSelectedNode() {
		if (dowMod==null) return null;
		Object lastComponent = tree.getLastSelectedPathComponent();
		if (lastComponent == null) return null;
		return (ModTreeNode) lastComponent;
	}
	
	public Integer getSelectionRow() {
		return tree.getSelectionModel().getMinSelectionRow();
	}
	
	public void setSelectionRow(int row) {
		tree.setSelectionRow(row);
	}
	
	
	public DowFolder getLastSelectedDowFolder() {
		ModTreeNode node = getLastSelectedNode();
		if (node==null) return null;
		return getLastSelectedNode().getDowFolder();
	}
	
	
	public ExpansionState getExpansionState() {
		return new ExpansionState();
	}
	
	public class ExpansionState {
		
		ArrayList<Integer> expList = new ArrayList<Integer>();
		
		public ExpansionState() {
			save();
		}
		
	    public ArrayList<Integer> save() {
	    	expList.clear();
	        for (int row=0; row<tree.getRowCount(); row++) {
	        	if (tree.isExpanded(row)) expList.add(row);
	        }
	        return expList;
	    }

	    public void load() {
	    	for (int i=0; i<expList.size(); i++){
	    		Integer row = expList.get(i);
	    		tree.expandRow(row);
	    	}
	    }
		
	}
	
}
