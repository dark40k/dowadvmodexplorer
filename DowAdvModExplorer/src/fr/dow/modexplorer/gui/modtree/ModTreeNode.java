package fr.dow.modexplorer.gui.modtree;

import java.util.ArrayList;

import fr.dow.gamedata.filesystem.DowFolder;

public class ModTreeNode {

	private ArrayList<ModTreeNode> childList = new ArrayList<ModTreeNode>();
	
	private DowFolder dowFolder;

	private String name;

	public ModTreeNode(DowFolder dowFolder, String name) {
		this.dowFolder=dowFolder;
		this.name = name;
		
		for (DowFolder child : dowFolder.getSubFoldersList().values()) {
			childList.add(new ModTreeNode(child, child.getName()));
		}
	}
	
	public DowFolder getDowFolder() {
		return dowFolder;
	}
	
	public String toString() {
		return name;
	}
	
	public int getChildCount() {
		return childList.size();
	}
	
	public ModTreeNode getChild(int index) {
		return childList.get(index);
	}
	
	public int getIndexOfChild(ModTreeNode node) {
		return childList.indexOf(node);
	}

}
