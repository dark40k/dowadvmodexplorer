package fr.dow.modexplorer.gui;

import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import fr.dow.modexplorer.modexplorerdata.RecentModsList;

public class RecentModMenu implements RecentModsList.UpdateRecentMapListListener {
	
	public static final String textNoMod="< Empty >"; 

	RecentModsList modList;
	
	JMenu jMenu;
	JMenuItem[] jRecentModItems; 
	
	public RecentModMenu(String title, RecentModsList modList, ActionListener listener) {
		
		this.modList=modList;
		
		jMenu=new JMenu(title);
		
		JMenuItem menuItem;
		jRecentModItems=new JMenuItem[RecentModsList.maxMods];
		for (int i=0;i<jRecentModItems.length;i++) {
			menuItem = new JMenuItem("");
	        menuItem.setActionCommand(textNoMod);
	        menuItem.addActionListener(listener);
	        
	        jRecentModItems[i]=menuItem;
	        jMenu.add(menuItem);
		}
		
		modList.addUpdateListener(this);

		updateSubMenuTitles();
		
	}
	
	private void updateSubMenuTitles() {
		
		String mapName;
		
		for (int i=0;i<RecentModsList.maxMods;i++) {
			
			try {
				mapName=modList.recentModsNames.get(i);
			}
			catch (ArrayIndexOutOfBoundsException e) {
				mapName=null;
			}
			
			if (mapName==null) {
				mapName=""; 
	        	jRecentModItems[i].setActionCommand(textNoMod);
			} else {
		        jRecentModItems[i].setActionCommand(mapName);
			}
			
	        jRecentModItems[i].setText(Integer.toString(i)+" - "+mapName);
	        
		}
		
	}
	
	public JMenu getJMenu() {
		return jMenu;
	}

	@Override
	public void recentMapListUpdated(RecentModsList.UpdateRecentMapListEvent evt) {
		updateSubMenuTitles();		
	}

	
}
