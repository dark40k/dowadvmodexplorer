package fr.dow.modexplorer.gui.replacetext;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.GridBagLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;

import fr.dow.modexplorer.gui.replacetext.replacetable.ReplaceTable;

public abstract class ReplaceTextDialogBase extends JDialog {

	private JPanel contentPanel;

	protected JLabel labelContext;
	protected JLabel labelMod;
	
	protected JTextField textSearchedString;
	protected JTextField textFileFilter;
	protected JCheckBox chckbxRecursiveSearch;

	protected ReplaceTable tableFound;
	
	protected JTextField textReplaceString;
	protected JCheckBox chckbxAutoChunkResize;
	protected JCheckBox chckbxAutoCreateFiles;
	
	//
	// Methodes internes
	//

	protected abstract void cmdUpdateSearch();
	protected abstract void cmdSelectAll();
	protected abstract void cmdCancel();
	protected abstract void cmdReplaceAll();
	protected abstract void cmdShowSearchPatternHelp();
	protected abstract void cmdClearAll();
	
	//
	// Affichage et constructeur
	//

	/** 
	 * Constructor.
	 */
	public ReplaceTextDialogBase(Window owner) {
		super(owner);
		initializeGUI();
	}

	/** 
	 * Create the dialog.
	 */
	private void initializeGUI() {
		
		setTitle("Dawn of War mass replace");

		setModalityType(ModalityType.DOCUMENT_MODAL);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		setBounds(100, 100, 550, 600);
		getContentPane().setLayout(new BorderLayout());

		{
			contentPanel = new JPanel();
			contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
			getContentPane().add(contentPanel, BorderLayout.CENTER);
			GridBagLayout gbl_contentPanel = new GridBagLayout();
			gbl_contentPanel.columnWidths = new int[]{127, 0};
			gbl_contentPanel.rowHeights = new int[]{46, 0, 0, 0};
			gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_contentPanel.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
			contentPanel.setLayout(gbl_contentPanel);

			{
				JPanel panel = new JPanel();
				panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Search parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				GridBagConstraints gbc_panel = new GridBagConstraints();
				gbc_panel.insets = new Insets(0, 0, 5, 0);
				gbc_panel.anchor = GridBagConstraints.NORTH;
				gbc_panel.fill = GridBagConstraints.HORIZONTAL;
				gbc_panel.gridx = 0;
				gbc_panel.gridy = 0;
				contentPanel.add(panel, gbc_panel);
				GridBagLayout gbl_panel = new GridBagLayout();
				gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
				gbl_panel.rowHeights = new int[]{0, 0, 0, 20, 0, 0};
				gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
				gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				panel.setLayout(gbl_panel);
				{
					JLabel lblMod = new JLabel("Mod :");
					GridBagConstraints gbc_lblMod = new GridBagConstraints();
					gbc_lblMod.anchor = GridBagConstraints.EAST;
					gbc_lblMod.insets = new Insets(0, 0, 5, 5);
					gbc_lblMod.gridx = 0;
					gbc_lblMod.gridy = 0;
					panel.add(lblMod, gbc_lblMod);
				}
				{
					labelMod = new JLabel("< No context available >");
					GridBagConstraints gbc_labelMod = new GridBagConstraints();
					gbc_labelMod.insets = new Insets(0, 0, 5, 5);
					gbc_labelMod.gridwidth = 2;
					gbc_labelMod.anchor = GridBagConstraints.WEST;
					gbc_labelMod.gridx = 1;
					gbc_labelMod.gridy = 0;
					panel.add(labelMod, gbc_labelMod);
				}
				{
					JLabel lblContext = new JLabel("Dow search folder :");
					GridBagConstraints gbc_lblContext = new GridBagConstraints();
					gbc_lblContext.anchor = GridBagConstraints.EAST;
					gbc_lblContext.insets = new Insets(0, 0, 5, 5);
					gbc_lblContext.gridx = 0;
					gbc_lblContext.gridy = 1;
					panel.add(lblContext, gbc_lblContext);
				}
				{
					labelContext = new JLabel("< No context available >");
					GridBagConstraints gbc_labelContext = new GridBagConstraints();
					gbc_labelContext.insets = new Insets(0, 0, 5, 0);
					gbc_labelContext.gridwidth = 2;
					gbc_labelContext.anchor = GridBagConstraints.WEST;
					gbc_labelContext.gridx = 1;
					gbc_labelContext.gridy = 1;
					panel.add(labelContext, gbc_labelContext);
				}
				{
					JLabel lblFileFilter = new JLabel("File filter :");
					GridBagConstraints gbc_lblFileFilter = new GridBagConstraints();
					gbc_lblFileFilter.anchor = GridBagConstraints.EAST;
					gbc_lblFileFilter.insets = new Insets(0, 0, 5, 5);
					gbc_lblFileFilter.gridx = 0;
					gbc_lblFileFilter.gridy = 2;
					panel.add(lblFileFilter, gbc_lblFileFilter);
				}
				{
					textFileFilter = new JTextField();
					textFileFilter.setText("*.{whm,whe}");
					textFileFilter.setColumns(10);
					GridBagConstraints gbc_textFileFilter = new GridBagConstraints();
					gbc_textFileFilter.insets = new Insets(0, 0, 5, 5);
					gbc_textFileFilter.fill = GridBagConstraints.HORIZONTAL;
					gbc_textFileFilter.gridx = 1;
					gbc_textFileFilter.gridy = 2;
					panel.add(textFileFilter, gbc_textFileFilter);
				}
				{
					JButton btnHelp = new JButton("Help");
					btnHelp.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							cmdShowSearchPatternHelp();
						}
					});
					GridBagConstraints gbc_btnHelp = new GridBagConstraints();
					gbc_btnHelp.insets = new Insets(0, 0, 5, 0);
					gbc_btnHelp.fill = GridBagConstraints.HORIZONTAL;
					gbc_btnHelp.gridx = 2;
					gbc_btnHelp.gridy = 2;
					panel.add(btnHelp, gbc_btnHelp);
				}
				{
					JLabel lblSearchString = new JLabel("Search string :");
					GridBagConstraints gbc_lblSearchString = new GridBagConstraints();
					gbc_lblSearchString.insets = new Insets(0, 0, 5, 5);
					gbc_lblSearchString.anchor = GridBagConstraints.EAST;
					gbc_lblSearchString.gridx = 0;
					gbc_lblSearchString.gridy = 3;
					panel.add(lblSearchString, gbc_lblSearchString);
				}
				{
					textSearchedString = new JTextField();
					GridBagConstraints gbc_textSearchedString = new GridBagConstraints();
					gbc_textSearchedString.insets = new Insets(0, 0, 5, 5);
					gbc_textSearchedString.fill = GridBagConstraints.HORIZONTAL;
					gbc_textSearchedString.gridx = 1;
					gbc_textSearchedString.gridy = 3;
					panel.add(textSearchedString, gbc_textSearchedString);
					textSearchedString.setColumns(10);
				}
				{
					JLabel lblSearchSubfolders = new JLabel("Search sub-folders :");
					GridBagConstraints gbc_lblSearchSubfolders = new GridBagConstraints();
					gbc_lblSearchSubfolders.insets = new Insets(0, 0, 5, 5);
					gbc_lblSearchSubfolders.gridx = 0;
					gbc_lblSearchSubfolders.gridy = 4;
					panel.add(lblSearchSubfolders, gbc_lblSearchSubfolders);
				}
				{
					chckbxRecursiveSearch = new JCheckBox("");
					chckbxRecursiveSearch.setSelected(true);
					GridBagConstraints gbc_chckbxRecursiveSearch = new GridBagConstraints();
					gbc_chckbxRecursiveSearch.insets = new Insets(0, 0, 5, 5);
					gbc_chckbxRecursiveSearch.anchor = GridBagConstraints.WEST;
					gbc_chckbxRecursiveSearch.gridwidth = 3;
					gbc_chckbxRecursiveSearch.gridx = 1;
					gbc_chckbxRecursiveSearch.gridy = 4;
					panel.add(chckbxRecursiveSearch, gbc_chckbxRecursiveSearch);
				}
			}

			{
				JPanel panel = new JPanel();
				panel.setBorder(new TitledBorder(null, "Found occurences", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				GridBagConstraints gbc_panel = new GridBagConstraints();
				gbc_panel.insets = new Insets(0, 0, 5, 0);
				gbc_panel.fill = GridBagConstraints.BOTH;
				gbc_panel.gridx = 0;
				gbc_panel.gridy = 1;
				contentPanel.add(panel, gbc_panel);
				GridBagLayout gbl_panel = new GridBagLayout();
				gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0};
				gbl_panel.rowHeights = new int[]{0, 50, 0};
				gbl_panel.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
				gbl_panel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
				panel.setLayout(gbl_panel);
				{
					JButton btnSelelectAll = new JButton("Select All");
					btnSelelectAll.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							cmdSelectAll();
						}
					});
					GridBagConstraints gbc_btnSelelectAll = new GridBagConstraints();
					gbc_btnSelelectAll.anchor = GridBagConstraints.NORTH;
					gbc_btnSelelectAll.insets = new Insets(0, 0, 5, 5);
					gbc_btnSelelectAll.gridx = 0;
					gbc_btnSelelectAll.gridy = 0;
					panel.add(btnSelelectAll, gbc_btnSelelectAll);
				}
				{
					JButton btnUpdateSearch = new JButton("Update Search");
					btnUpdateSearch.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							cmdUpdateSearch();
						}
					});
					{
						JButton btnClearAll = new JButton("Clear All");
						btnClearAll.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								cmdClearAll();
							}
						});
						GridBagConstraints gbc_btnClearAll = new GridBagConstraints();
						gbc_btnClearAll.insets = new Insets(0, 0, 5, 5);
						gbc_btnClearAll.gridx = 1;
						gbc_btnClearAll.gridy = 0;
						panel.add(btnClearAll, gbc_btnClearAll);
					}
					GridBagConstraints gbc_btnUpdateSearch = new GridBagConstraints();
					gbc_btnUpdateSearch.anchor = GridBagConstraints.NORTHWEST;
					gbc_btnUpdateSearch.insets = new Insets(0, 0, 5, 0);
					gbc_btnUpdateSearch.gridx = 3;
					gbc_btnUpdateSearch.gridy = 0;
					panel.add(btnUpdateSearch, gbc_btnUpdateSearch);
				}
				{
					tableFound = new ReplaceTable();
					GridBagConstraints gbc_scrollPane = new GridBagConstraints();
					gbc_scrollPane.gridwidth = 4;
					gbc_scrollPane.fill = GridBagConstraints.BOTH;
					gbc_scrollPane.gridx = 0;
					gbc_scrollPane.gridy = 1;
					panel.add(tableFound, gbc_scrollPane);
				}
			}

			{
				JPanel panel = new JPanel();
				panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Replace parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				GridBagConstraints gbc_panel = new GridBagConstraints();
				gbc_panel.anchor = GridBagConstraints.NORTH;
				gbc_panel.fill = GridBagConstraints.HORIZONTAL;
				gbc_panel.gridx = 0;
				gbc_panel.gridy = 2;
				contentPanel.add(panel, gbc_panel);
				GridBagLayout gbl_panel = new GridBagLayout();
				gbl_panel.columnWidths = new int[]{0, 0, 0};
				gbl_panel.rowHeights = new int[]{20, 0, 0, 0};
				gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
				gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
				panel.setLayout(gbl_panel);
				{
					JLabel lblNewLabel = new JLabel("Replace string :");
					GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
					gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
					gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
					gbc_lblNewLabel.gridx = 0;
					gbc_lblNewLabel.gridy = 0;
					panel.add(lblNewLabel, gbc_lblNewLabel);
				}
				{
					textReplaceString = new JTextField();
					GridBagConstraints gbc_textReplaceString = new GridBagConstraints();
					gbc_textReplaceString.insets = new Insets(0, 0, 5, 0);
					gbc_textReplaceString.fill = GridBagConstraints.HORIZONTAL;
					gbc_textReplaceString.gridx = 1;
					gbc_textReplaceString.gridy = 0;
					panel.add(textReplaceString, gbc_textReplaceString);
					textReplaceString.setColumns(10);
				}
				{
					JLabel lblAutoUpdateString = new JLabel("RelicChunk auto resize :");
					lblAutoUpdateString.setToolTipText("Updates string and chunk sizes for RelicChunk");
					GridBagConstraints gbc_lblAutoUpdateString = new GridBagConstraints();
					gbc_lblAutoUpdateString.anchor = GridBagConstraints.EAST;
					gbc_lblAutoUpdateString.insets = new Insets(0, 0, 5, 5);
					gbc_lblAutoUpdateString.gridx = 0;
					gbc_lblAutoUpdateString.gridy = 1;
					panel.add(lblAutoUpdateString, gbc_lblAutoUpdateString);
				}
				{
					chckbxAutoChunkResize = new JCheckBox((String) null);
					chckbxAutoChunkResize.setSelected(true);
					GridBagConstraints gbc_chckbxAutoChunkResize = new GridBagConstraints();
					gbc_chckbxAutoChunkResize.insets = new Insets(0, 0, 5, 5);
					gbc_chckbxAutoChunkResize.anchor = GridBagConstraints.WEST;
					gbc_chckbxAutoChunkResize.gridx = 1;
					gbc_chckbxAutoChunkResize.gridy = 1;
					panel.add(chckbxAutoChunkResize, gbc_chckbxAutoChunkResize);
				}
				{
					JLabel lblAutoCreateFiles = new JLabel("Auto create files :");
					GridBagConstraints gbc_lblAutoCreateFiles = new GridBagConstraints();
					gbc_lblAutoCreateFiles.anchor = GridBagConstraints.EAST;
					gbc_lblAutoCreateFiles.insets = new Insets(0, 0, 5, 5);
					gbc_lblAutoCreateFiles.gridx = 0;
					gbc_lblAutoCreateFiles.gridy = 2;
					panel.add(lblAutoCreateFiles, gbc_lblAutoCreateFiles);
				}
				{
					chckbxAutoCreateFiles = new JCheckBox((String) null);
					chckbxAutoCreateFiles.setSelected(true);
					GridBagConstraints gbc_chckbxAutoCreateFiles = new GridBagConstraints();
					gbc_chckbxAutoCreateFiles.insets = new Insets(0, 0, 5, 5);
					gbc_chckbxAutoCreateFiles.anchor = GridBagConstraints.WEST;
					gbc_chckbxAutoCreateFiles.gridx = 1;
					gbc_chckbxAutoCreateFiles.gridy = 2;
					panel.add(chckbxAutoCreateFiles, gbc_chckbxAutoCreateFiles);
				}
			}



		}

		// Panneau inferieur : boutons OK/Cancel
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			GridBagLayout gbl_buttonPane = new GridBagLayout();
			gbl_buttonPane.columnWidths = new int[]{0, 0, 0};
			gbl_buttonPane.rowHeights = new int[]{23, 0};
			gbl_buttonPane.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
			gbl_buttonPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			buttonPane.setLayout(gbl_buttonPane);
			{
				JButton replaceAllButton = new JButton("Replace All");
				replaceAllButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cmdReplaceAll();
					}
				});
				replaceAllButton.setActionCommand("OK");
				GridBagConstraints gbc_replaceAllButton = new GridBagConstraints();
				gbc_replaceAllButton.anchor = GridBagConstraints.NORTHWEST;
				gbc_replaceAllButton.insets = new Insets(5, 5, 5, 5);
				gbc_replaceAllButton.gridx = 0;
				gbc_replaceAllButton.gridy = 0;
				buttonPane.add(replaceAllButton, gbc_replaceAllButton);
				getRootPane().setDefaultButton(replaceAllButton);
			}
			{
				JButton closeButton = new JButton("Cancel");
				closeButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cmdCancel();
					}
				});
				closeButton.setActionCommand("Cancel");
				GridBagConstraints gbc_closeButton = new GridBagConstraints();
				gbc_closeButton.insets = new Insets(5, 5, 5, 5);
				gbc_closeButton.anchor = GridBagConstraints.NORTHEAST;
				gbc_closeButton.gridx = 1;
				gbc_closeButton.gridy = 0;
				buttonPane.add(closeButton, gbc_closeButton);
			}
		}

		
	}
	

}
