package fr.dow.modexplorer.gui.replacetext;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JEditorPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FileFilterHelpDialog extends JDialog {

	private final static java.net.URL helpURL = ReplaceTextDialog.class.getResource("filterhelp.html");
	
	private final JPanel contentPanel = new JPanel();

	JEditorPane editorPane;
	
	public void cmdClose() {
		this.setVisible(false);
	}
	
	public static void showDialog() {
		FileFilterHelpDialog dialog = new FileFilterHelpDialog();
		dialog.setVisible(true);
	}
	
	/**
	 * Create the dialog.
	 */
	public FileFilterHelpDialog() {
		setTitle("File filter help");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JScrollPane scrollPane = new JScrollPane();
			contentPanel.add(scrollPane, BorderLayout.CENTER);
			{
				editorPane = new JEditorPane();
				editorPane.setEditable(false);
				scrollPane.setViewportView(editorPane);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Close");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cmdClose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		if (helpURL != null) {
		    try {
		        editorPane.setPage(helpURL);
		    } catch (IOException e) {
		        System.err.println("Attempted to read a bad URL: " + helpURL);
		    }
		} else {
		    System.err.println("Couldn't find file: filterhelp.html");
		}
		
	}

}
