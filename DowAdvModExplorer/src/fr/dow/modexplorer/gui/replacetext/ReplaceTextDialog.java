package fr.dow.modexplorer.gui.replacetext;

import java.awt.Window;
import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.modexplorer.replacetext.ReplaceList;

public class ReplaceTextDialog extends ReplaceTextDialogBase {

	ReplaceList list = new ReplaceList();
	private DowMod dowMod;
	private DowFolder dowFolder;
	
	//
	// Methodes internes
	//

	protected void cmdUpdateSearch() {
		list.findDowFilesToReplace(dowMod, dowFolder, chckbxRecursiveSearch.isSelected(), textSearchedString.getText(), textFileFilter.getText());
		tableFound.setReplaceList(list, "", dowMod);
	}

	protected void cmdSelectAll() {
		list.setReplace(true);
	}
	
	protected void cmdClearAll() {
		list.setReplace(false);
	}
	
	protected void cmdCancel() {
		this.setVisible(false);
	}
	
	protected void cmdReplaceAll() {
		list.doReplace(textReplaceString.getText(), chckbxAutoCreateFiles.isSelected(), chckbxAutoChunkResize.isSelected());
	}

	protected void cmdShowSearchPatternHelp() {
		FileFilterHelpDialog.showDialog();
	}
	
	//
	// Utilities
	//
	
	
	//
	// Affichage et constructeur
	//

	/**
	 * Show dialog to make mass replacement in a mod
	 * 
	 * @param owner : window owning the modal dialog
	 * @param dowFolder 
	 */
	public static void showDialog(Window owner, DowMod dowMod, DowFolder dowFolder) {
		try {
			ReplaceTextDialog dialog = new ReplaceTextDialog(owner, dowMod, dowFolder);
			dialog.setVisible(true);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** 
	 * Constructor.
	 */
	public ReplaceTextDialog(Window owner, DowMod dowMod, DowFolder dowFolder) {
		super(owner);
		
		this.dowMod=dowMod;
		this.dowFolder=dowFolder;
		
		labelContext.setText(dowFolder.getDowPath());
		labelMod.setText(dowMod.getModFile().getAbsolutePath());

	}

}
