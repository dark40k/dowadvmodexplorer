package fr.dow.modexplorer.gui.replacetext.replacetable;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.modexplorer.replacetext.ReplaceList;
import fr.dow.modexplorer.replacetext.ReplaceList.UpdateReplaceListEvent;

public class ReplaceTableModel extends AbstractTableModel implements ReplaceList.ReplaceListListener {

	private ReplaceList replaceList;
	
	private static final String[] columnNames = { "Repl.", "Nbr.", "Support", "RelicChunk", "File Name", "Dow Path", "Rank", "Storing position" };

	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}
    
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		if (replaceList==null) return 0;
		return replaceList.size();
	};

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		if (replaceList == null)
			return null;
		if (rowIndex > replaceList.size())
			return null;

		switch (columnIndex) {
		case 0:
			return replaceList.get(rowIndex).getReplace();
		case 1:
			return replaceList.get(rowIndex).getCount();
		case 2:
			return (replaceList.get(rowIndex).getDowFileVersion().getFile()!=null) ? "File" : "Archive";
		case 3:
			return (replaceList.get(rowIndex).isRelicChunk()) ? "Yes" : "No";
		case 4:
			return replaceList.get(rowIndex).getDowFileVersion().getDowFile().getName();
		case 5:
			return replaceList.get(rowIndex).getDowFileVersion().getDowFile().getDowPath();
		case 6:
			DowFileVersion version = replaceList.get(rowIndex).getDowFileVersion();
			return version.getDowFile().getRank(version);
		case 7:
			return (replaceList.get(rowIndex).getDowFileVersion().getFile()!=null)
					? replaceList.get(rowIndex).getDowFileVersion().getGlobalPath() 
					: replaceList.get(rowIndex).getDowFileVersion().getSgaArchive().get_SgaFile().getAbsolutePath();
		}

		return null;

	}
	
	@Override
    public Class<?> getColumnClass(int c) {
		if (c==0) return Boolean.class;
		return String.class;
    }

	@Override
    public boolean isCellEditable(int row, int col) {
      return (col == 0);
    }

	@Override
    public void setValueAt(Object value, int row, int col) {
		replaceList.get(row).setReplace((Boolean) value);
    }
	
	//
	// Getters
	//
	
	public DowFileVersion getDowFileVersion(int rowModel) {
		return replaceList.get(rowModel).getDowFileVersion();
	}

	//
	// Methods for managing table datas
	//
	
	public void setReplaceList(ReplaceList list) {
		
		if (replaceList!=null) replaceList.removeUpdateListener(this);
		
		this.replaceList=list;
		
		list.addUpdateListener(this);
		
		fireTableDataChanged();
	}

	//
	// Replacelist changed event
	//
	@Override
	public void replaceListUpdated(UpdateReplaceListEvent evt) {
		fireTableDataChanged();
	}


}