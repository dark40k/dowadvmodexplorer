package fr.dow.modexplorer.gui.replacetext.replacetable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;

import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.modexplorer.gui.modfoldertable.ModFolderTable;
import fr.dow.modexplorer.gui.utils.TableColumnAdjuster;
import fr.dow.modexplorer.replacetext.ReplaceList;

public class ReplaceTable extends JPanel {
	
	//private static final Color overwrittenFileVersionColor=ModFolderTable.overwrittenFileVersionColor;
	private static final Color archivedFileVersionColor=ModFolderTable.archivedFileVersionColor;
	private static final Color higherModFileVersionColor=ModFolderTable.higherModFileVersionColor;
	private static final Color editableFileVersionColor=ModFolderTable.editableFileVersionColor;
		
	private JTable table;
	private ReplaceTableModel tableModel;
	private DowMod dowMod;
	private DowFolder dowFolder;
	private TableColumnAdjuster tca;
	
	public ReplaceTable() {
		
		table = new JTable() {
		    public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
		        {
		            Component c = super.prepareRenderer(renderer, row, column);

		            if (row==getSelectedRow()) return c;
		            
		            int rowModel = convertRowIndexToModel(row);
		            
		            if (tableModel.getDowFileVersion(rowModel).getFile()==null)
		            	c.setBackground(archivedFileVersionColor);
		            else if (!tableModel.getDowFileVersion(rowModel).getFile().getAbsolutePath().startsWith(dowMod.getModFolder().getAbsolutePath()))
		            	c.setBackground(higherModFileVersionColor);
		            else
		            	c.setBackground(editableFileVersionColor);
		            
		            return c;
		        }
		    };
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		tableModel = new ReplaceTableModel();
		table.setModel(tableModel);
		
		table.setShowGrid(true);

		table.setAutoCreateRowSorter(true);
		
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tca = new TableColumnAdjuster(table);
		tca.adjustColumns();
		
		setLayout(new BorderLayout(0, 0));
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(table);
		add(scrollPane, BorderLayout.CENTER);
	}

	
	
	//
	// Setters
	//

	public void setReplaceList(ReplaceList list,String basePath, DowMod dowMod) {
		
		this.dowMod=dowMod;

		tableModel.setReplaceList(list);
		tca.adjustColumns();
		
	}
	 
	//
	// Getters
	//
	
	public Object getFolder() {
		return dowFolder;
	}
		
}
