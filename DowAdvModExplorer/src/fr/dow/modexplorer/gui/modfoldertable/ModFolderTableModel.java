package fr.dow.modexplorer.gui.modfoldertable;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import fr.dow.gamedata.filesystem.DowFile;
import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;

public class ModFolderTableModel extends AbstractTableModel {

	private static final String[] columnNames = { "Type", "File Name", "Rank", "Storing position" };

	private class FileData {
		String type;
		String name;
		Integer rank;
		String file;
		Boolean isLast;
		DowFileVersion dowFileVersion;
	}
	
	private Vector<FileData> fileDataLines=new Vector<FileData>();
	
	//
	// Methods from AbstractTableModel that need to be Overriden
	//
	
    public String getColumnName(int col) { return columnNames[col]; }
    
	@Override
	public int getColumnCount() { return 4; }

	@Override
	public int getRowCount() { return fileDataLines.size(); };

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if (fileDataLines==null) return null;
		if (rowIndex>fileDataLines.size()) return null;
		
		switch (columnIndex) {
		case 0 : return fileDataLines.get(rowIndex).type; 
		case 1 : return fileDataLines.get(rowIndex).name; 
		case 2 : return fileDataLines.get(rowIndex).rank; 
		case 3 : return fileDataLines.get(rowIndex).file; 
		}
		
		return null;
		
	}
	
	//
	// Getters
	//
	public Boolean isLastVersion(int rowIndex) {
		return fileDataLines.get(rowIndex).isLast;
	}
	
	public File getFile(int rowModel) {
		return fileDataLines.get(rowModel).dowFileVersion.getFile();
	}
	
	//
	// Methods for managing table datas
	//
	
	public DowFileVersion getDowFileVersion(int rowModel) {
		FileData mapData=fileDataLines.get(rowModel);
		if (mapData==null) return null;
		return mapData.dowFileVersion;
	}
	
	public void clear() {
		fileDataLines.clear();
		fireTableDataChanged();
	}
	
	public void addMapFolder(DowFolder mapFolder, DowMod topMod, boolean onlyTop, boolean viewLowerVersions, boolean viewLowerMods, boolean viewArchives) {
		
		String dowBasePath = topMod.getModFolder().getParentFile().getAbsolutePath();
		String modBasePath = topMod.getModFolder().getAbsolutePath();
		
		Vector<DowFile> mapDowFileList=mapFolder.getSubDowFileList(new ModuleFilter());
		
		for (DowFile dowFile : mapDowFileList) {
			
			int maxRank=dowFile.getVersionsCount();
			
			for (int i=maxRank-1; i>=0; i--) {
				
				if (!viewLowerVersions)
					if (i!=maxRank-1)
						continue;
				
				if (!viewLowerMods) {
					File baseFile = dowFile.getDowVersion(i).getFile();
					if (baseFile==null) baseFile = dowFile.getDowVersion(i).getSgaArchive().get_SgaFile();
					
					if (!baseFile.getAbsolutePath().startsWith(modBasePath))
						continue;
				}

				if (!viewArchives)
					if (dowFile.getDowVersion(i).getSgaArchive()!=null)
							continue;
				
				FileData newMapData=new FileData();
				
				newMapData.name=dowFile.getName();
				newMapData.rank=i;
				
				newMapData.isLast=(i==maxRank-1);
				
				newMapData.dowFileVersion=dowFile.getDowVersion(i);
				newMapData.type=(dowFile.getDowVersion(i).getFile()!=null)?"File":"Archive";
				
				if (newMapData.dowFileVersion.getFile()!=null) newMapData.file=newMapData.dowFileVersion.getFile().getAbsolutePath().replace(dowBasePath, ".");
				if (newMapData.dowFileVersion.getSgaArchive()!=null) newMapData.file=newMapData.dowFileVersion.getSgaArchive().get_SgaFile().getAbsolutePath().replace(dowBasePath, ".");
				
				fileDataLines.add(newMapData);
				
				if (onlyTop) break;
				
			}
			
		}
		
		Collections.sort(
				fileDataLines, 
				new Comparator<FileData>() {
					public int compare(FileData mapData1, FileData mapData2) {
						int comp=mapData1.name.compareTo(mapData2.name);
						if (comp!=0) return comp;
						return mapData1.rank.compareTo(mapData2.rank);
				  }
				});
		
		fireTableDataChanged();
	}

	//
	//
	//
	
	private static class ModuleFilter implements FilenameFilter {
		public boolean accept(File dir, String name) {
//			int pos=name.lastIndexOf('.');
//			if (pos<0) return false;
//			return name.substring(pos).equals(".sgb");
			return true;
		}
	}

	
}