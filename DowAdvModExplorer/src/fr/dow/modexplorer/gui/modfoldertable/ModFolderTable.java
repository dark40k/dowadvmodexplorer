package fr.dow.modexplorer.gui.modfoldertable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.EventListener;
import java.util.EventObject;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;

import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.modexplorer.gui.utils.TableColumnAdjuster;

public class ModFolderTable extends JPanel {
	
	public static final Color overwrittenFileVersionColor=new Color(204,204,204);
	public static final Color archivedFileVersionColor=Color.WHITE;
	public static final Color higherModFileVersionColor=Color.WHITE;
	public static final Color editableFileVersionColor=Color.cyan;
	
//	private static final Color overwrittenFileVersionColor=new Color(204,204,204);
//	private static final Color archivedFileVersionColor=new Color(255,255,204);
//	private static final Color higherModFileVersionColor=new Color(255,255,204);
//	private static final Color editableFileVersionColor=Color.WHITE;
	
	private JTable table;
	private ModFolderTableModel tableModel;
	private DowMod dowMod;
	private DowFolder dowFolder;
	private TableColumnAdjuster tca;
	
	public ModFolderTable() {
		
		table = new JTable(){
		    public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
		        {
		            Component c = super.prepareRenderer(renderer, row, column);

		            if (row==getSelectedRow()) return c;
		            
		            int rowModel = convertRowIndexToModel(row);
		            
		            if (!tableModel.isLastVersion(rowModel))
		            	c.setBackground(overwrittenFileVersionColor);
		            else if (tableModel.getFile(rowModel)==null)
		            	c.setBackground(archivedFileVersionColor);
		            else if (!tableModel.getFile(rowModel).getAbsolutePath().startsWith(dowMod.getModFolder().getAbsolutePath()))
		            	c.setBackground(higherModFileVersionColor);
		            else
		            	c.setBackground(editableFileVersionColor);
		            
		            return c;
		        }
		    };
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		tableModel = new ModFolderTableModel();
		table.setModel(tableModel);
		
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setShowGrid(true);

		table.setAutoCreateRowSorter(true);
		
		tca = new TableColumnAdjuster(table);
		tca.adjustColumns();

//		table.getColumnModel().getColumn(0).setPreferredWidth(25);
//		table.getColumnModel().getColumn(1).setPreferredWidth(250);
//		table.getColumnModel().getColumn(2).setPreferredWidth(25);
//		table.getColumnModel().getColumn(3).setPreferredWidth(400);

		table.addMouseListener(mouseListener);

		setLayout(new BorderLayout(0, 0));
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(table);
		add(scrollPane, BorderLayout.CENTER);
	}

	//
	// Setters
	//

	public void setFolder(DowFolder dowFolder, DowMod dowMod, boolean viewLowerVersions, boolean viewLowerMods, boolean viewArchives) {

		this.dowFolder=dowFolder;
		this.dowMod=dowMod;
		
		tableModel.clear();
		
		if (dowFolder == null) return;
		
		// System.out.println("Selecting " + dowFolder.getDowPath());
		
		tableModel.addMapFolder(dowFolder, dowMod, false, viewLowerVersions, viewLowerMods, viewArchives);
		
		tca.adjustColumns();
		
	}

	//
	// Getters
	//
	
	public Object getFolder() {
		return dowFolder;
	}
	
	public DowFileVersion getSelectedDowFileVersion() {
		
		if (dowFolder==null) return null;
		
		int selTableRow=table.getSelectedRow();
		if (selTableRow<0) return null;
		
		int selModelRow = table.convertRowIndexToModel(selTableRow);
		if (selModelRow<0) return null;
		
		return tableModel.getDowFileVersion(table.convertRowIndexToModel(selModelRow));
	}
	
	//
	// Selection listener
	//
	public void addListSelectionListener(ListSelectionListener tableSelectionListener) {
		table.getSelectionModel().addListSelectionListener(tableSelectionListener);
	}

	public void removeListSelectionListener(ListSelectionListener tableSelectionListener) {
		table.getSelectionModel().removeListSelectionListener(tableSelectionListener);
	}
	
	//
	// DblClick listener
	//
	
	private MouseAdapter mouseListener =  new MouseAdapter() {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() != 2) return;
			
			DowFileVersion selVersion = getSelectedDowFileVersion();
			if (selVersion==null) return;
			
			fireDblClickEvent(selVersion);
		}
	};

/*	private ListSelectionListener listSelectionListener = new ListSelectionListener() {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			
	        if (e.getValueIsAdjusting()) return;

	        ListSelectionModel lsm = (ListSelectionModel) e.getSource();
	        
	        if (lsm.isSelectionEmpty()) return;
	        
	        //table.requestFocus();
	        
		}
		
	}; */

	//
	// DblClick events
	//
	
	private EventListenerList dblClickListenerList = new EventListenerList();

	public class UpdateDblClickEvent extends EventObject {
		DowFileVersion dowFileVersion;
		public UpdateDblClickEvent(Object source, DowFileVersion dowFileVersion){
			super(source);
			this.dowFileVersion=dowFileVersion;
		};
		public DowFileVersion getDowFileVersion() {
			return dowFileVersion;
		}
	}

	public interface DblClickListener extends EventListener {
		public void dblClicked(UpdateDblClickEvent evt);
	}

	public void addDblClickListener(DblClickListener listener) {
		dblClickListenerList.add(DblClickListener.class, listener);
	}
	
	public void removeDblClickListener(DblClickListener listener) {
		dblClickListenerList.remove(DblClickListener.class, listener);
	}
	
	public void fireDblClickEvent(DowFileVersion dowFileVersion) {
		UpdateDblClickEvent evt=new UpdateDblClickEvent(this,dowFileVersion);
		Object[] listeners = dblClickListenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == DblClickListener.class) {
				((DblClickListener) listeners[i+1]).dblClicked(evt);
			}
		}
	}

}
