package fr.dow.modexplorer.utils.console;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;

import fr.dow.modexplorer.utils.console.MessageConsole.ConsoleOutputStream;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.PrintStream;
import java.sql.Timestamp;
import java.awt.Font;

public class MessageConsoleDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MessageConsoleDialog dialog = new MessageConsoleDialog();
			dialog.setVisible(true);
			
			System.out.println(new Timestamp(System.currentTimeMillis())+" - Holy Rusty Metal Batman! I can't believe this was so simple!");
			System.err.println(new Timestamp(System.currentTimeMillis())+" - God I hate you Robin.");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public MessageConsoleDialog() {
		
		setTitle("Console output");
		setBounds(100, 100, 1200, 500);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPanel.add(scrollPane, BorderLayout.CENTER);
		JTextPane textPane = new JTextPane();
		textPane.setFont(new Font("Consolas", Font.PLAIN, 13));
		scrollPane.setViewportView(textPane);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		JButton okButton = new JButton("Close");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cmdOK();
			}
		});
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);

		
		//
		// Log messages into console
		//
		
		MessageConsole mc = new MessageConsole(textPane);
		
		ConsoleOutputStream cosOut = mc.new ConsoleOutputStream(null, null);
		MultiOutputStream multiOut= new MultiOutputStream(System.out, cosOut);
		PrintStream stdout= new PrintStream(multiOut,true);
		System.setOut(stdout);

		ConsoleOutputStream cosErr = mc.new ConsoleOutputStream(Color.RED, null);
		MultiOutputStream multiErr= new MultiOutputStream(System.err, cosErr);
		PrintStream stderr= new PrintStream(multiErr,true);
		System.setErr(stderr);
		
		mc.setMessageLines(5000);
		
	}

	private void cmdOK() {
		this.setVisible(false);
	}
	
}
