package fr.dow.modexplorer.replacetext;

import java.util.Arrays;

import fr.dow.gamedata.RelicChunkDATA;

public class ReplaceChunkData {

	public static class AutoResizeFailedException extends Exception {
	    public AutoResizeFailedException(String message) {
	        super(message);
	    }
	}
	
	public static int replaceStringInChunkDATA(RelicChunkDATA chunk, String searchPatternString, String replacePatternString, boolean autoResizeString) throws AutoResizeFailedException {
		
		if (chunk == null) return 0;
		
		byte[] searchPattern = searchPatternString.getBytes();
		byte[] replacePattern = replacePatternString.getBytes();
		
		byte [] byteArray = chunk.dataArray();
				
		int count = 0;
		
		int index=0;
		while (true) {
			index = findData(byteArray, searchPattern,index,byteArray.length-1);
	
			if (index < 0) break;
			
			count++;
			
			byteArray=replaceData(byteArray, index, index+searchPattern.length, replacePattern);
			
			if (autoResizeString) {
				
				// scan upward for 1st 0x00
				int sizeIndex=index;
				while (sizeIndex>=0) { if (byteArray[sizeIndex]==0) break; sizeIndex--; }
				
				// move 3 bytes up to get start of size block
				sizeIndex=sizeIndex-3;
				if (sizeIndex<0) {
					throw new AutoResizeFailedException("Initial 4 bytes length not found.");
				}
				
				// load block size
				int size = byteArray[sizeIndex + 3] << 24 | (byteArray[sizeIndex + 2] & 0xff) << 16
						| (byteArray[sizeIndex + 1] & 0xff) << 8 | (byteArray[sizeIndex + 0] & 0xff);

				// update size
				size=size - searchPattern.length +  replacePattern.length;
				
				// write new size
				byteArray[sizeIndex+0]=(byte) ((size & 0x000000FF) >> 0);
				byteArray[sizeIndex+1]=(byte) ((size & 0x0000FF00) >> 8);
				byteArray[sizeIndex+2]=(byte) ((size & 0x00FF0000) >> 16);
				byteArray[sizeIndex+3]=(byte) ((size & 0xFF000000) >> 24);
				
			}
			
			index=index+replacePattern.length;
		}
		
		chunk.dataWrapArray(byteArray);
		
		chunk.updateSize();
		
		return count;
	}

	private static int findData(byte[] data, byte[] pattern, int start, int end) {
    	int index=findData(Arrays.copyOfRange(data, start, end+1), pattern);
    	if (index<0) return -1;
    	return index+start;
    }	

    
    private static byte[] replaceData(byte[] data, int start, int end, byte[] replacePattern) {
    	
    	byte[] newData = new byte[data.length + start - end + replacePattern.length ];
    	
    	for (int i=0;i<start;i++) newData[i]=data[i];
    	
    	for (int i=0;i<replacePattern.length;i++) newData[start+i]=replacePattern[i];
    	
    	int i1 = start - end + replacePattern.length;
    	for (int i=end; i<data.length; i++) newData[i+i1]=data[i];
    	
    	return newData;
    }
    
    /**
     * Search the data byte array for the first occurrence
     * of the byte array pattern.
     */
    private static int findData(byte[] data, byte[] pattern) {
        int[] failure = computeFailure(pattern);

        int j = 0;

        for (int i = 0; i < data.length; i++) {
            while (j > 0 && pattern[j] != data[i]) {
                j = failure[j - 1];
            }
            if (pattern[j] == data[i]) {
                j++;
            }
            if (j == pattern.length) {
                return i - pattern.length + 1;
            }
        }
        return -1;
    }
    
    /**
     * Computes the failure function using a boot-strapping process,
     * where the pattern is matched against itself.
     */
    private static int[] computeFailure(byte[] pattern) {
        int[] failure = new int[pattern.length];

        int j = 0;
        for (int i = 1; i < pattern.length; i++) {
            while (j>0 && pattern[j] != pattern[i]) {
                j = failure[j - 1];
            }
            if (pattern[j] == pattern[i]) {
                j++;
            }
            failure[i] = j;
        }

        return failure;
    }

	
	
}
