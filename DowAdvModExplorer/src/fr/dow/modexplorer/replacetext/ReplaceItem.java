package fr.dow.modexplorer.replacetext;

import fr.dow.gamedata.filesystem.DowFileVersion;

public class ReplaceItem {

	private Boolean replace;
	
	private DowFileVersion dowFileVersion; 
	
	private Integer count;

	private Boolean isRelicChunk;
	
	public ReplaceItem(DowFileVersion dowFileVersion, Integer foundNbr, Boolean isRelicChunk) {
		this.dowFileVersion=dowFileVersion;
		this.count=foundNbr;
		this.isRelicChunk=isRelicChunk;
		setReplace(true);
	}
	
	public void addCount(Integer foundNbr) {
		this.count+=foundNbr;
	}

	public Integer getCount() {
		return count;
	}
	
	public Boolean getReplace() {
		return replace;
	}

	public Boolean isRelicChunk() {
		return isRelicChunk;
	}
	
	public void setReplace(Boolean replace) {
		this.replace = replace;
	}

	public DowFileVersion getDowFileVersion() {
		return dowFileVersion;
	}


}
