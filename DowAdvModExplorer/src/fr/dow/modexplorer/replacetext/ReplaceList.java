package fr.dow.modexplorer.replacetext;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.PathMatcher;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.event.EventListenerList;

import fr.dow.gamedata.RelicChunk;
import fr.dow.gamedata.RelicChunkDATA;
import fr.dow.gamedata.RelicChunkFOLD;
import fr.dow.gamedata.RelicChunkFile;
import fr.dow.gamedata.filesystem.DowFile;
import fr.dow.gamedata.filesystem.DowFileVersion;
import fr.dow.gamedata.filesystem.DowFolder;
import fr.dow.gamedata.filesystem.DowMod;
import fr.dow.modexplorer.ModExplorer;
import fr.dow.modexplorer.replacetext.ReplaceChunkData.AutoResizeFailedException;

public class ReplaceList {

	private static final String CHUNK_HEADER_STRING = new String(RelicChunkFile.FILEHEADER);
	
	private ArrayList<ReplaceItem> foundList = new ArrayList<ReplaceItem>();
	private HashMap<String,Integer> pathList = new HashMap<String,Integer>();
	
	private DowMod dowMod;
	private String searchString;
	private String filePattern;
	
	//
	// Getters
	//
	
	public int size() {
		return foundList.size();
	}
	
	public ReplaceItem get(int index) {
		return foundList.get(index);
	}
	
	//
	// Methodes
	//
	
	public void findDowFilesToReplace(DowMod dowMod, DowFolder dowFolder, Boolean scanSubFolders, String searchString, String filePattern) {
		
		this.dowMod=dowMod;
		this.searchString=searchString;
		this.filePattern=filePattern;
		
		foundList.clear();
		pathList.clear();
		
		findDowFilesToReplace(dowFolder,scanSubFolders);
		
		fireReplaceListUpdateEvent();

	}
	
	private void findDowFilesToReplace(DowFolder dowFolder, Boolean scanSubFolders) {
		
		final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:"+filePattern);
		
		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return matcher.matches(new File(dir,name).toPath());
			}
		};
		
		Vector<DowFile> listDowFiles = dowFolder.getSubDowFileList(filter);
		
		for (DowFile dowFile : listDowFiles) {
			
			try {
				System.out.println("Scanning : " + dowFile.getDowPath());
				
				DowFileVersion dowFileVersion = dowFile.getLastVersion();
				
				String data = new String(dowFileVersion.getData());
				
				boolean isRelicChunk = data.startsWith(CHUNK_HEADER_STRING);
				
				int count;
				if (isRelicChunk)  {
					count = chunkFileReplace(dowFileVersion, searchString, searchString, false, false);
				} else {
					count = countMatches(data,searchString);
				}
				
				if (count>0) 
					addDowFileVersion(dowFileVersion, count, isRelicChunk);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		if (scanSubFolders) {
			for (DowFolder subDowFolder : dowFolder.getSubFoldersList().values()) {
				findDowFilesToReplace(subDowFolder,  scanSubFolders);
			}
		}
		
	}
	
	public void setReplace(Boolean value) {
		for (ReplaceItem item : foundList) item.setReplace(value);
		fireReplaceListUpdateEvent();
	}
	
	public void doReplace(String replaceString, boolean createFile, boolean autoChunkResize) {

		if (replaceString==null) {
			System.out.println("Replace String is empty. Aborting.");
			return;
		}
		
		for (ReplaceItem item : foundList)
			if (item.getReplace()) {

				DowFileVersion dowFileVersion = item.getDowFileVersion();

				//System.out.println("Replacing file : " + dowFileVersion.getDowFile().getDowPath());

				boolean createFileNeeded = (dowFileVersion.getFile() == null);

				if (!createFileNeeded) {
					createFileNeeded = !(dowFileVersion.getFile().getAbsolutePath().startsWith(dowMod.getModFolder().getAbsolutePath()));
				}
				
				if (createFileNeeded) {
					
					System.out.println("Creating file : " + dowFileVersion.getDowFile().getDowPath());
					
					if (!createFile) {
						System.err.println("File does not exist. Skipping");
						continue;
					} else {
						

						File newFile = ModExplorer.createFile(dowFileVersion, dowMod);

						if (newFile == null) {
							System.err.println("Error, could not create file. Skipping");
							continue;
						}

						dowFileVersion = dowFileVersion.getDowFile().getLastVersion();
					}
					
				}

				if (item.isRelicChunk()) {
					// TODO Make replacement
					//System.err.println("Relic chunk file not implemented, skipping");
					chunkFileReplace(dowFileVersion, searchString, replaceString, autoChunkResize, true);
				} else {
					// TODO Make replacement
					//System.err.println("Raw replace file not implemented, skipping");
					rawReplace(dowFileVersion, searchString, replaceString);
				}
				

			}
	}
	
	//
	// Replace for raw files
	//
	
	private void rawReplace(DowFileVersion dowFileVersion, String searchString, String replaceString) {
		
		System.out.println("Raw Replace for file : " + dowFileVersion.getDowFile().getDowPath());
		
		try {
			
			int count=0;
			
			String data = new String(dowFileVersion.getData());
			
			int pos=0;
			while(true) {
				pos = data.indexOf(searchString, pos);
				if (pos<0) break;
				
				data = ((pos>0) ? data.substring(0, pos) : "") + replaceString + data.substring(pos+searchString.length());
				
				pos+=replaceString.length();
				count++;
			}
			
			File fileOut = new File(dowMod.getAbsoluteFilePath(dowFileVersion.getDowFile().getDowPath()));
			FileOutputStream fos = new FileOutputStream(fileOut);
			fos.write(data.getBytes());
			fos.close();

			System.out.println("Done. Nbr of replacements = " + count);
			
		} catch (IOException e) {
			System.err.println("Failed, Skipping");
		}
		
	}
	
	//
	// Replace for chunk files
	//
	
	private int chunkFileReplace(DowFileVersion dowFileVersion, String searchString, String replaceString, Boolean autoChunkResize, Boolean doReplace) {
		
		String introMsg = (doReplace) ? "Chunk Replace for file : " : "Chunk count replace for file : ";
		
		System.out.println(introMsg + dowFileVersion.getDowFile().getDowPath());
		
		int count=0;
		
		try {
			
			RelicChunkFile chunkFile = new RelicChunkFile(dowFileVersion);
			
			for (RelicChunk chunk : chunkFile ) count+=chunkReplace(chunk, searchString, replaceString,autoChunkResize);
			
			if (doReplace) chunkFile.save(dowFileVersion.getFile());
			
			System.out.println("Done. Nbr of replacements = " + count);
			
		} catch (IOException e) {
			System.err.println("Error accessing file, Skipping.");
		} catch (AutoResizeFailedException e) {
			System.err.println("Error resizing string, Skipping.");
		}
		
		return count;
		
	}
	
	private int chunkReplace(RelicChunk chunk, String searchString, String replaceString, Boolean autoChunkResize) throws AutoResizeFailedException {
		
		int count = 0;
		
		if (chunk.get_name().contains(searchString)) {
			String data=chunk.get_name();
			int pos=0;
			while(true) {
				pos = data.indexOf(searchString, pos);
				if (pos<0) break;
				data = ((pos>0) ? data.substring(0, pos) : "") + replaceString + data.substring(pos+searchString.length());
				pos+=replaceString.length();
				count++;
			}
			chunk.set_name(data);
			chunk.updateSize();
		}
		
		if (chunk instanceof RelicChunkFOLD) {
			RelicChunkFOLD chunkFOLD=(RelicChunkFOLD) chunk;
			for (RelicChunk subChunk : chunkFOLD.get_subChunks() ) count+=chunkReplace(subChunk, searchString, replaceString, autoChunkResize);
			chunkFOLD.updateSize();
			return count;
		}
		
		if (chunk instanceof RelicChunkDATA) {
			RelicChunkDATA chunkDATA=(RelicChunkDATA) chunk;
			return count + ReplaceChunkData.replaceStringInChunkDATA(chunkDATA, searchString, replaceString, autoChunkResize);
		}
		
		throw new IllegalArgumentException("chunk is neither RelicChunkFOLD nor RelicChunkDATA");
		
	}
	
	//
	// Utilities
	//
	
	private static String keyName(DowFileVersion dowFileVersion) {
		if (dowFileVersion==null) return null;
		return dowFileVersion.getDowFile().getDowPath()+"/"+dowFileVersion.getDowFile().getName();
	}

	
	private static int countMatches(String string, String substring) {
		if (string==null) return 0;
		if (substring==null) return 0;
		
		int count=0;
		int pos=0;
		
		while(true) {
			pos = string.indexOf(substring, pos);
			if (pos<0) break;
			count++;
			pos+=substring.length();
		}
		
		return count;
	}
	
	private void addDowFileVersion(DowFileVersion dowFileVersion, Integer foundNbr, boolean isRelicChunk) {
		
		String key = keyName(dowFileVersion);
		
		if (pathList.containsKey(key)) {
			foundList.get(pathList.get(key)).addCount(foundNbr);
		} else {
			ReplaceItem item=new ReplaceItem(dowFileVersion,foundNbr,isRelicChunk);
			foundList.add(item);
			pathList.put(key,foundList.indexOf(item));
		}
		
	}
	
	
	//
	// Change event
	//
	
	private EventListenerList listenerList = new EventListenerList();

	public class UpdateReplaceListEvent extends EventObject {
		public UpdateReplaceListEvent(Object source) {
			super(source);
		}
	}

	public interface ReplaceListListener extends EventListener {
		public void replaceListUpdated(UpdateReplaceListEvent evt);
	}

	public void addUpdateListener(ReplaceListListener listener) {
		listenerList.add(ReplaceListListener.class, listener);
	}
	
	public void removeUpdateListener(ReplaceListListener listener) {
		listenerList.remove(ReplaceListListener.class, listener);
	}
	
	public void fireReplaceListUpdateEvent() {
		UpdateReplaceListEvent evt=new UpdateReplaceListEvent(this);
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == ReplaceListListener.class) {
				((ReplaceListListener) listeners[i+1]).replaceListUpdated(evt);
			}
		}
	}

	
}
