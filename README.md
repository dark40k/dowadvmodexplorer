# Dow Adv Mod Explorer  #
[AdvModExplorer thread on RelicNews Forum](http://forums.relicnews.com/showthread.php?274157)
## Basic ##
The goal of this tool is to allow anyone to have a good view of the content of a mod.
Basically, you load the ".module" file in the root folder and will see every file contained in the mod no matter if the file is on the disc or in an sga archive.
## Features ##
* view all dow formats with appropriate viewer (FFE, picture, 3D model,...)
* extract reference files in local mod from archives to allow modding
* perform mass replacement (and creation if needed) of files
* ...
## [Download section](https://bitbucket.org/dark40k/dowadvmodexplorer/downloads) ##
![ModExplorer_v0.04-02.png](https://bitbucket.org/repo/eAp7xL/images/2328084848-ModExplorer_v0.04-02.png)